Rails.application.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    resources :data_sheets
    resources :faqs do
      collection do
        post 'update_faq_order'
      end
    end
    resources :files_in_directory, only: [:index]
    resources :contact_mailer, only: [:create]
    resources :users do
      collection do
        get 'user_name'
        post 'admin_create_account'
      end
    end
    resources :roles
    resources :products
    resources :quote, only: [:create]
    resources :categories
    resources :sessions do
      collection do
        post 'login'
        post 'reset_password'
        get 'home'
        get 'profile'
        get 'setting'
      end
    end
    resources :files do
      collection do
        post 'upload'
        post 'download'
        post 'delete'
        post 'delete_public_attachment'
      end
    end
    resources :rmas do
      collection do
        post 'public_upload'
        post 'create_used_part'
        post 'send_update_email'
        post 'delete_rma'
      end
    end
    resources :statuses
    #resources :sessions
  end

  root 'application#index', anchor: false

  # get '*path' => 'application#index'
end