var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('AccountsCtrl', function ($scope, $rootScope, $timeout, $state, $http, $location, $window, $stateParams, AuthService) {
    'use strict';

    $http.get('/api/users').success(function (data, status) {
        $scope.newAccounts = _.where(data, {active: false});
        $scope.oldAccounts = _.where(data, {active: true});
    }).error(function (data, status) {
        console.log(status, data)

    }).then(function () {
        console.log($scope.newAccounts)
        if ($scope.newAccounts == null || $scope.newAccounts.length < 1) {
            $('#newAccountsTable').DataTable();
        }
    });

    $scope.$on("$viewContentLoaded", function () {

    });
});

hvdiagnostics.directive('oldAccountsPostRepeat', function ($timeout) {
    return function(scope, element, attrs) {
        if (scope.$last){
            $timeout(function () {
                $('#oldAccountsTable').DataTable();
            }, 0);
        }
    };
});

hvdiagnostics.directive('newAccountsPostRepeat', function ($timeout) {
    return function(scope, element, attrs) {
        if (scope.$last){
            $timeout(function () {
                $('#newAccountsTable').DataTable();
            }, 0);
        }
    };
});