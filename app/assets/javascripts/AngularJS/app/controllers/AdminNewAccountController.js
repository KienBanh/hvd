var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('AdminCreateAccountController', function ($scope, $rootScope, $timeout, $http, $filter, $state, $stateParams) {
    "use strict";

    $scope.newUser = {};
    $scope.userExists = false;

    $scope.adminCreateAccount = function () {
        console.log($scope.adminCreateAccountForm);

        $http.post("/api/users/admin_create_account", $scope.newUser).success(function (data, status) {
            console.log(data, status)

            if (data.status_code == 1) {
                alert(data.msg);

                $state.go("account.accountsTable");
            } else if (data.status_code == -2) {
                alert(data.msg);

                $scope.userExists = true;
                $scope.adminCreateAccountForm.$submitted = false;
            } else {
                alert(data.msg);
                $scope.adminCreateAccountForm.$submitted = false;
            }
        }).error(function (data, status) {
            console.log(data, status)
            alert(data.msg);
            $scope.adminCreateAccountForm.$submitted = false;
        });
    }
});