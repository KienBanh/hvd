var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller("editFAQsCtrl", function ($rootScope, $scope, $http, $timeout) {
    "use strict";

    $http.get('/api/faqs').success(function (data, status) {
        $rootScope.faqs = data;
    }).error(function (data, status) {

    });

    var questionEditor = new nicEditor({
        iconsPath : '/images/nicEditorIcons.gif',
        buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'fontSize', 'fontFamily', 'fontFormat', 'subscript', 'superscript', 'link']
    });

    var answerEditor = new nicEditor({
        iconsPath : '/images/nicEditorIcons.gif',
        buttonList : ['bold', 'italic', 'underline', 'ol', 'ul', 'fontSize', 'fontFamily', 'fontFormat', 'subscript', 'superscript', 'link']
    });

    $scope.editFAQ = function (faq) {
        if (faq) {
            $scope.faqToEdit = faq;
        } else {
            $scope.faqToEdit = {};
        }

        $('#editFAQModal').modal({
            closable: false,
            onShow: function () {
                $timeout(function () {
                    //questionEditor.panelInstance('question');
                    answerEditor.panelInstance('answer');
                }, 0)
            },
            onHidden: function () {
                //questionEditor.removeInstance('question');
                answerEditor.removeInstance('answer');
            },
            onApprove: function ($element) {
                //$scope.faqToEdit.question = nicEditors.findEditor('question').getContent();
                $scope.faqToEdit.question = $scope.faqToEdit.question.toUpperCase();
                $scope.faqToEdit.answer = nicEditors.findEditor('answer').getContent();
                $scope.faqSubmitted = true;

                $http.put('/api/faqs/' + $scope.faqToEdit.id, $scope.faqToEdit).success(function (data, status) {
                    console.log(data, status);

                    if (data.status == 1) {
                        $("#faqsTable").DataTable().destroy();

                        $scope.faqs = [];
                        $scope.faqs = data.faqs;

                        return true;
                    } else {
                        alert(data.msg);

                        return false;
                    }

                }).error(function (data, status) {
                    alert(data.msg);

                    return false;
                });

                $scope.faqSubmitted = false;
            }
        }).modal('show');
    }

    $scope.deleteFAQ = function (faq) {
        $scope.faqSubmitted = true;

        $http.delete("/api/faqs/" + faq.id).success(function (data, status) {
            if (data.status == 1) {
                $("#faqsTable").DataTable().destroy();

                $scope.faqs = data.faqs;

                $('#editFAQModal').modal({
                    onHidden: function () {
                        //questionEditor.removeInstance('question');
                        answerEditor.removeInstance('answer');
                    }
                }).modal('hide');
            } else {
                alert(data.msg);
            }
        }).error(function (data, status) {
            alert(data.msg);
        });

        $scope.faqSubmitted = false;
    }
})

hvdiagnostics.directive("faqsTableRepeat", function ($timeout, $http, $rootScope) {
    return function (scope, element, attrs) {
        if (scope.$last){
            if (!$.fn.DataTable.isDataTable('#faqsTable')) {
                $timeout(function () {
                    $("#faqsTable").DataTable({
                        "paging": false,
                        select: true,
                        "ordering": false
                    });
                }, 0);
            } else {
                $timeout(function () {
                    //$("#faqsTable").DataTable({
                    //    "paging": false,
                    //    select: true,
                    //    "ordering": false
                    //});
                }, 0);
            }

            $("#sortable").disableSelection();

            $("#sortable").sortable({
                start: function(event, ui) {
                    ui.item.startPos = ui.item.index();
                },
                stop: function (event, ui) {
                    $http.post('/api/faqs/update_faq_order', {id: ui.item.attr("data-id"), startPos: ui.item.startPos + 1, endPos: ui.item.index()+ 1}).success(function (data, status) {
                        if (data.status == 1) {
                            $rootScope.faqs = data.faqs;
                        } else if (data.status == -1) {
                            alert(data.msg);
                        }
                    }).error(function (data, status) {
                        alert(data.msg);
                    });
                }
            });
        }
    }
});