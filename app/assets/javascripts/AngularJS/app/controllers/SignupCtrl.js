var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('SignupCtrl', function ($scope, $rootScope, $timeout, $state, $http, $location, $window, $compile) {
    'use strict';

    $http.get('/api/products').success(function (data, status) {
        $scope.products = data;
    });

    $scope.user = {};
    $scope.user.products = [];
    $scope.user.products.push({});

    $scope.$on('$viewContentLoaded', function () {
        $('.dropdown').dropdown();
    });

    $rootScope.$on('$viewContentLoading', function(event, viewConfig){
    });

    $scope.submitSignup = function () {
        $scope.signupFormSubmitted = true;
        $scope.signupForm.$submitted = true;

        if ($scope.signupForm.$valid == true) {
            $scope.user.email_address = $scope.user.email_address.toLowerCase();
            $http.post('/api/users', $scope.user).success(function (data, status) {
                console.log(data)
                if (data.active == false) {
                    $('#accountCreatedModal').modal("show").modal({onApprove: function ($element) {
                        $state.go('main.login')
                    }});
                } else if (data.active == true) {
                    $('#accountActivatedModal').modal("show").modal({
                        onApprove: function ($element) {
                            $state.go('main.login')
                        }
                    });
                } else if (data == -2) {
                    $scope.userExists = true;
                    $scope.signupFormSubmitted = false;
                } else {
                    $('#accountErrorModal').modal("show").modal({onApprove: function ($element) {
                        //$scope.signupForm.$submitted = false;

                        //$state.go('main.signup', {reload: true});
                    }});
                    $scope.signupFormSubmitted = false;
                }
            }).error(function (data, status) {
                $('#accountErrorModal').modal("show");
                $scope.signupFormSubmitted = false;
                //$scope.signupForm.$submitted = false;
            });
        } else {
            $scope.signupFormSubmitted = false;
        }
    }

    $scope.removeProduct = function (index) {
        if ($scope.user.products.length > 1) {
            $scope.user.products.splice(index, 1);
        }
    }

    $scope.countryChange = function() {
        if ($scope.user.country === 'CA') {
            $scope.user.state = "";
        }
    }
});

hvdiagnostics.directive("compareTo", function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});