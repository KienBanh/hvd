var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('SoftwareCtrl', function ($scope, $rootScope, $timeout, $http, categories, $filter, products, user) {
    $rootScope.$on("$viewContentLoaded", function () {

    });

    $scope.products = products.data;
    $scope.categories = categories.data;
    $scope.user = user.data;

    angular.forEach($scope.categories, function(object, index) {
        $http.get("/api/files_in_directory?path=Products/"+object.name+"/Software").success(function (data, status) {
            object.software = data;
        });
    });

    //$http.get("/api/products").success(function (data, status) {
    //    $scope.products = data;
    //});

    //$http.get("/api/users/" + $rootScope.user.id).success(function (data, status) {
    //    $scope.user = data;
    //});

    $scope.getCategoriesName = function (id) {
        var category = _.findWhere(categories.data, {id: parseInt(id)})

        if (category){
            return category.name;
        } else {
            return "Other"
        }
    }

    $scope.show = function (category) {
        if ($rootScope.user.role_id == 2) {
            return true;
        }

        var productsInCategory = _.where($scope.products, {category_id: category.id});
        var userOwnsAProductInCategory;

        _.each($scope.user.products_user, function (value, key, list) {

            if (!userOwnsAProductInCategory) {
                userOwnsAProductInCategory = _.findWhere(productsInCategory, {id: value.product_id});
            }
        });

        if (userOwnsAProductInCategory) {
            return true;
        } else {
            return false;
        }
    }
});