var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('AdminEditRmaController', function($scope, $rootScope, $timeout, $http, $filter, $state, $stateParams, FileUploader) {
    $scope.used_parts = {};
    $scope.shipping_material = [{
            name: 'Pallet',
            selected: false
        },
        {
            name: 'Cardboard Box',
            selected: false
        },
        {
            name: 'Crate',
            selected: false
        },
        {
            name: 'Transport Case',
            selected: false
        },
        {
            name: 'Other',
            selected: false,
            value: null
        },
    ];

    $scope.$on("$viewContentLoaded", function() {
        $(".datepicker").datepicker();
    })

    $http.get('/api/statuses').success(function(data, status) {
        $scope.statuses = data;
    });

    $http.get('/api/products').success(function(data, status) {
        $scope.products = data;
    });

    $scope.getRma = function() {
        $http.get("/api/rmas/" + $stateParams.id).success(function(data, status) {
        if (data.status == 1) {
            $scope.rma = data.rma;
            $scope.rmaClone = angular.copy($scope.rma);
            $scope.attachments = data.attachments;
            $scope.receivedParts = data.received_parts;
            $scope.receivedPartsClone = angular.copy($scope.receivedParts);
            $scope.rmaClone.repair_completed_on = $filter('date')($scope.rmaClone.repair_completed_on, "shortDate");
            $scope.rmaClone.received_on = $filter('date')($scope.rmaClone.received_on, "shortDate");
            $scope.otherPartsInput = _.findWhere($scope.receivedPartsClone, { other: true }) ? _.findWhere($scope.receivedPartsClone, { other: true }).part : "";
            $scope.otherShippingMaterialInput = $scope.rma.shipping_material;
            $scope.rmaPublicMessages = _.where(data.rmaMessages, { private: false });
            $scope.rmaInternalMessages = _.where(data.rmaMessages, { private: true });
            $scope.usedParts = data.used_parts;

            // parse shipping_material from JSON
            try {
                if ($scope.rma.shipping_material) {
                    $scope.shipping_material = JSON.parse($scope.rma.shipping_material);
                }
            } catch (e) {
                // legacy support
                var shipping_material = _.find($scope.shipping_material, function(material) {
                    return material.name === $scope.rma.shipping_material;
                });

                if (shipping_material) {
                    shipping_material.selected = true;
                } else {
                    if ($scope.rma.shipping_material) {
                        var other = _.find($scope.shipping_material, function(material) {
                            return material.name === "Other";
                        });
                        other.selected = true;
                        other.value = $scope.rma.shipping_material;
                    }
                }
            }

            if (_.findWhere($scope.receivedPartsClone, { other: true })) {
                $scope.otherParts = true;
            }
        } else {
            $scope.rma = {};
            $scope.attachments = [];
        }
    }).error(function(data, status) {

    });
    }

    $scope.getRma();

    $scope.editRmaInformation = function() {
        $('.ui.modal.editRmaInformationModal')
            .modal('show').modal({
                onApprove: function(element) {
                    if ($scope.rmaClone.repair_completed_on) {
                        $scope.rmaClone.repair_completed_on = new Date($scope.rmaClone.repair_completed_on);
                    }

                    if ($scope.rmaClone.received_on) {
                        $scope.rmaClone.received_on = new Date($scope.rmaClone.received_on);
                    }

                    $http.put("/api/rmas/" + $stateParams.id, $scope.rmaClone).success(function(data, status) {
                        if (data.status == 1) {
                            $scope.rma = data.rma;
                            $scope.rmaClone = angular.copy($scope.rma);
                            $scope.attachments = data.attachments;
                            $scope.receivedParts = data.received_parts;
                            $scope.receivedPartsClone = angular.copy($scope.receivedParts);
                            $scope.rmaClone.repair_completed_on = $filter('date')($scope.rmaClone.repair_completed_on, "shortDate");
                            $scope.rmaClone.received_on = $filter('date')($scope.rmaClone.received_on, "shortDate");
                        }

                    }).error(function(data, status) {

                    }).then(function() {
                        // $state.go($state.current, {}, { reload: true });     
                        // $state.reload();           
                    });
                }
            });
    }

    $scope.editReceivedEquipmentInformation = function() {
        $('.ui.modal.editReceivedEquipmentInformationModal')
            .modal('show').modal({
                onApprove: function(element) {
                    if ($scope.rmaClone.received_on) {
                        $scope.rmaClone.received_on = new Date($scope.rmaClone.received_on);
                    }

                    if ($scope.rmaClone.repair_completed_on) {
                        $scope.rmaClone.repair_completed_on = new Date($scope.rmaClone.repair_completed_on);
                    }

                    // combine shipping_material into one(1) string
                    $scope.rmaClone.shipping_material = JSON.stringify($scope.shipping_material);

                    $http.put("/api/rmas/" + $stateParams.id, { rma: $scope.rmaClone, received_parts: $scope.receivedPartsClone }).success(function(data, status) {
                        if (data.status == 1) {
                            $scope.rma = data.rma;
                            $scope.rmaClone = angular.copy($scope.rma);
                            $scope.attachments = data.attachments;
                            $scope.receivedParts = data.received_parts;
                            $scope.receivedPartsClone = angular.copy($scope.receivedParts);
                            $scope.rmaClone.repair_completed_on = $filter('date')($scope.rmaClone.repair_completed_on, "shortDate");
                            $scope.rmaClone.received_on = $filter('date')($scope.rmaClone.received_on, "shortDate");
                        }
                    }).error(function(data, status) {

                    });
                }
            });
    }

    $scope.toggleReceivedParts = function(part) {
        var partFound = _.findWhere($scope.receivedPartsClone, { part: part });
        var otherParts = _.findWhere($scope.receivedPartsClone, { other: true });

        if (part == "Other") {
            $scope.otherParts = !$scope.otherParts;

            if ($scope.otherParts == false) {
                $scope.receivedPartsClone = _.without($scope.receivedPartsClone, _.findWhere($scope.receivedPartsClone, { other: true }));
            } else {
                $scope.receivedPartsClone.push({ part: $scope.otherPartsInput, other: true });
            }
        } else {
            // is currently selected
            if (partFound) {
                $scope.receivedPartsClone = _.without($scope.receivedPartsClone, _.findWhere($scope.receivedPartsClone, { part: part }));
            }

            // is newly selected
            else {
                $scope.receivedPartsClone.push({ part: part, other: false });
            }
        }
    }

    $scope.partCheck = function(part) {
        if (part === "Other") {
            if (_.findWhere($scope.receivedPartsClone, { other: true })) {
                return true;
            } else {
                return false;
            }
        } else {
            if (_.findWhere($scope.receivedPartsClone, { part: part })) {
                return true
            } else {
                return false;
            }
        }
    }

    $scope.updateOtherPartsInput = function() {
        _.findWhere($scope.receivedPartsClone, { other: true }).part = $scope.otherPartsInput;
    }

    $(".ui.search.conditionSearch").search({
        source: [{
            title: "Acceptable"
        }, {
            title: "Poor"
        }],
        onSelect: function(result, response) {
            $scope.rmaClone.condition = result.title;
        }
    });

    $scope.addHvDiagnosticsFindings = function() {

        $http.put("/api/rmas/" + $stateParams.id, { rma: $scope.rma, rmaMessage: { message: $scope.hvDiagnosticsFindingsTextarea, private: false, user_id: $rootScope.user.id } }).success(function(data, status) {
            if (data.status == 1) {
                $scope.rmaPublicMessages = _.where(data.rmaMessages, { private: false });
                $scope.hvDiagnosticsFindingsTextarea = "";
            }

            $state.go($state.current, {}, { reload: true });
        }).error(function(data, status) {

        });
    }

    $scope.updateRepairCost = function() {
        if ($scope.rma.parts_labor_cost.length == 0) {
            $scope.rma.parts_labor_cost = 0;
        }

        if ($scope.rma.hours_spent.length == 0) {
            $scope.rma.hours_spent = 0;
        }

        if ($scope.rma.rate.length == 0) {
            $scope.rma.rate = 0;
        }

        if ($scope.rma.calibration_cost.length == 0) {
            $scope.rma.calibration_cost = 0;
        }

        $http.put("/api/rmas/" + $stateParams.id, $scope.rma).success(function(data, status) {}).error(function(data, status) {

        });
    }

    $scope.addHvDiagnosticsInternalsMessage = function() {
        $http.put("/api/rmas/" + $stateParams.id, { rma: $scope.rma, rmaMessage: { message: $scope.internalMessageTextarea, private: true, user_id: $rootScope.user.id } }).success(function(data, status) {
            if (data.status == 1) {
                $scope.rmaInternalMessages = _.where(data.rmaMessages, { private: true });
                $scope.internalMessageTextarea = "";
            }

            $state.go($state.current, {}, { reload: true });
        }).error(function(data, status) {

        });
    }


    $scope.uploader = new FileUploader({
        url: "/api/rmas/public_upload",
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
        formData: [
            { id: $stateParams.id }
        ],
        removeAfterUpload: true,
        onCompleteItem: function(item, response, status, headers) {
            $("#addPublicAttachmentInput").val("");
            $(".publicAttachmentButton").removeClass("loading").removeClass("disabled");

            if (response.status == 1) {
                $scope.attachments = response.attachments;
                alert(response.msg);
                return;
            }

            if (response.msg) {
                alert(response.msg);
            }

            if (status == 413) {
                alert("The file you are uploading is too large.");
            }
        },
        onBeforeUploadItem: function(item) {
            $(".publicAttachmentButton").addClass("loading").addClass("disabled");

            if (item.file.size > 5000000) {
                return;
            }

            if ($scope.attachments.length >= 12) {
                return;
            }
        }
    });

    $scope.uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|pdf|'.indexOf(type) !== -1;
        }
    });

    $scope.usedPartUploader = new FileUploader({
        url: "/api/rmas/create_used_part",
        headers: {
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
        formData: [
            { id: $stateParams.id, user_id: $rootScope.user.id, name: $scope.used_parts.name, description: $scope.used_parts.description }
        ],
        removeAfterUpload: true,
        onBeforeUploadItem: function(item) {
            $(".partsUploaderButton").addClass("loading").addClass("disabled");
            item.formData[0].name = $scope.used_parts.name;
            item.formData[0].description = $scope.used_parts.description;
        },
        onAfterAddingFile: function(item) {
            item.formData[0].name = $scope.used_parts.name;
            item.formData[0].description = $scope.used_parts.description;
        },
        onCompleteItem: function(item, response, status, headers) {
            $("#usedPartAttachmentInput").val("");
            $(".partsUploaderButton").removeClass("loading").removeClass("disabled");

            if (response.status == 1) {
                $("#usedPartsName").val("");
                $("#usedPartsDescription").val("");
                $scope.usedParts = response.used_parts;
                alert(response.msg);
                return;
            }

            console.log(status, response);

            if (response.msg) {
                alert(response.msg);
            }

            if (status == 413) {
                alert("The file you are uploading is too large.");
            }
        }
    });

    $scope.usedPartUploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/ , options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|pdf|'.indexOf(type) !== -1;
        }
    });

    $scope.addUsedPart = function() {
        if ($scope.usedPartForm.$valid) {
            //$scope.usedPartUploader.formData.push({id: $stateParams.id, user_id: $rootScope.user.id, name: $scope.used_parts.name, description: $scope.used_parts.description});
            if ($scope.usedPartUploader.queue.length > 0) {
                $scope.usedPartUploader.queue[0].upload();
            } else {
                $http.post("/api/rmas/create_used_part", { id: $stateParams.id, user_id: $rootScope.user.id, name: $scope.used_parts.name, description: $scope.used_parts.description }).success(function(data, status) {
                    if (data.status == 1) {
                        $scope.usedParts = data.used_parts;
                        $("#usedPartsName").val("");
                        $("#usedPartsDescription").val("");
                        $("#usedPartAttachmentInput").val("");
                    }

                    $state.go($state.current, {}, { reload: true });
                }).error(function(data, status) {

                })
            }
        }
    }

    $scope.subtotal = function() {
        if ($scope.rma) {
            if (parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.parts_labor_cost)) {
                return parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.parts_labor_cost);
            } else {
                return
            }
        }
    }

    $scope.total = function() {
        if ($scope.rma) {
            if (parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.calibration_cost) + parseFloat($scope.rma.parts_labor_cost)) {
                return parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.calibration_cost) + parseFloat($scope.rma.parts_labor_cost)
            } else {
                return
            }
        }
    }

    $scope.sendUpdateEmail = function() {
        $http.post("/api/rmas/send_update_email", $scope.rma).success(function(data, status) {
            console.log(data);

            alert(data.msg);
        }).error(function(data, status) {
            console.log(data);

            alert(data.msg);
        });
    };

    $scope.diffDays = function(onBenchDate, shippedBackDate) {
        console.log(shippedBackDate, onBenchDate)

        if (onBenchDate && shippedBackDate) {
            var onBenchDate = new Date(onBenchDate);
            var shippedBackDate = new Date(shippedBackDate);
            var oneDay = 24 * 60 * 60 * 1000;

            if (shippedBackDate.getTime() - onBenchDate.getTime() > 0) {
                return Math.round(Math.abs((shippedBackDate.getTime() - onBenchDate.getTime()) / (oneDay)));
            } else {
                return 0;
            }
        } else {
            return "?";
        }
    }

    $scope.deleteRma = function() {
        var r = confirm("You are about to delete this RMA.");

        if (r == true) {
            $http.post('/api/rmas/delete_rma', $scope.rma).success(function(data, status) {
                if (data.status === 1) {
                    $state.go('account.manageRmas');
                } else {
                    alert(data.msg);
                }
            });
        } else {
            return;
        }
    }

    $scope.isImageType = function(path) {
        if (path) {
            var type = path.split('.').pop();
            var types = ['jpg', 'png', 'jpeg', 'bmp', 'gif', 'JPEG', 'JPG'];

            if (types.indexOf(type) >= 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    $scope.isPdfType = function(path) {
        if (path) {
            var type = path.split('.').pop();
            var types = ['pdf'];

            if (types.indexOf(type) >= 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    $scope.removeAttachment = function(attachment) {
        $http.post('api/files/delete_public_attachment', attachment)
        .success(function() {
            $scope.getRma();
        });
    }
});