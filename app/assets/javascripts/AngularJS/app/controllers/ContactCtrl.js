'use strict';

var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('ContactCtrl', function($rootScope, $scope, $timeout, $http, $state) {
    $scope.contact = {};

    $timeout(function () {
        $('select.dropdown').dropdown();
    }, 0);

    $scope.submitContactForm = function () {
        $http.post('/api/contact_mailer', $scope.contact).success(function (data, status) {
            if (data.error_code == 0) {
                $('.ui.modal.success')
                    .modal('show')
                ;
            } else {
                $('.ui.modal.error')
                    .modal('show')
                ;
            }
        }).error(function (data, status) {
            $('.ui.modal.error')
                .modal('show')
            ;
        }).then(function () {
            $state.go('main.contact', {}, {reload: true})
        })
    }
});