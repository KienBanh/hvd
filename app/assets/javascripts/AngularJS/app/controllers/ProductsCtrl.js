var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('ProductsCtrl', function ($scope, $rootScope, $timeout, $state, $http) {
    'use strict';

    if (!$rootScope.quote) {
        $rootScope.quote = {};
    }

    $http.get('/api/products').success(function (data, status) {
        $scope.products = data;
    })

    $scope.$on('$viewContentLoaded', function () {

        $timeout(function () {
            $('.ui.accordion').accordion({exclusive: false});
            $('.ui.dropdown').dropdown();
            $('.productImageCarousel').slick({
                arrows: false,
                autoplay: true
            });
        }, 0);
    });

    $scope.requestQuote = function (product) {
        if (!$rootScope.quote.products) {
            $rootScope.quote.products = [];
            $rootScope.quote.products.push(product);
        } else {
            $rootScope.quote.products.push(product);
        }

        $state.go('main.quote', {}, {reload: false});
    }

    $scope.requestQuoteSubmit = function () {
        if ($scope.quoteForm.$valid) {
            $scope.submitting = true;
            $http.post('/api/quote', $rootScope.quote). success(function (data, status) {
                if (data.error_code == 0) {
                    alert('Your request for a quote has been submitted! We will be in contact soon!');
                    $rootScope.quote = {};
                    //$('.ui.dropdown').dropdown('clear');
                    $scope.submitting = false;
                }
            }).error(function (data, status) {
                $scope.submitting = false;
            });
        }
    }

    $scope.openModal = function (modalName) {
        $('.modal.'+modalName+'').modal('show');
    }

    $scope.getDataSheet = function (productName) {
        if ($scope.products) {
            var product = _.findWhere($scope.products, {name: productName});

            return product.data_sheet
        }
    }
});