var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('EditAccountCtrl', function ($scope, $rootScope, $timeout, $state, $http, $location, $window, $stateParams, AuthService) {
    'use strict';

    $scope.new_products_user = [];
    $scope.new_products_user.push({});

    $rootScope.$on('$viewContentLoaded', function(event, viewConfig){
        $http.get('/api/users/' + $stateParams.id).success(function (data, status) {
            $scope.userEdit = JSON.parse(data.user);
            $scope.email_address = $scope.userEdit.email_address;
            $scope.products_user = data.products_user;
        });
    });

    $scope.submitAccountEdit = function () {
        $scope.formSubmitted = true;
        $scope.accountEditForm.$submitted = true;

        if ($scope.accountEditForm.$valid) {
            $http.put('/api/users/' + $scope.userEdit.id, {user: $scope.userEdit, task: "updateAccount"}).success(function (data, status) {
                if (data.status_code == 1) {
                    $('#updateSuccess').modal("show").modal({onApprove: function ($element) {
                        //$state.go('main.login')
                    }});
                } else {
                    $('#updateError').modal("show").modal({onApprove: function ($element) {
                        //$state.go('main.login')
                    }});
                }
            }).error(function (data, status) {
                $('#updateError').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            });

        }

        $scope.formSubmitted = false;
    }

    $scope.submitChangeEmailAddressForm = function () {
        $scope.formSubmitted = true;
        $scope.changeEmailAddressForm.$submitted = true;

        if ($scope.changeEmailAddressForm.$valid) {
            $http.put('/api/users/' + $scope.userEdit.id, {
                user: $scope.userEdit,
                newEmailAddress: $scope.email_address,
                task: "updateEmailAddress"
            }).success(function (data, status) {
                $scope.changeEmailMsg = data.msg;

                if (data.status_code == -1) {
                    $scope.userExists = true;
                } else {
                    $('#changeEmailResultModal').modal("show").modal({onApprove: function ($element) {
                        //$state.go('main.login')
                    }});
                }
            }).error(function (data, status) {
                $scope.changeEmailMsg = data.msg;

                $('#changeEmailResultModal').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            });
        }

        $scope.formSubmitted = false;
    }

    $scope.submitChangePasswordForm = function () {
        $scope.formSubmitted = true;
        $scope.changePasswordForm.$submitted = true;

        if ($scope.changePasswordForm.$valid) {
            $http.put('/api/users/' + $scope.userEdit.id, {
                user: $scope.userEdit,
                newPassword: $scope.changePassword,
                task: "changePassword"
            }).success(function (data, status) {
                $scope.changePasswordMsg = data.msg;

                if (data.status_code == 1) {
                    $('#resetPasswordSuccess').modal("show").modal({onApprove: function ($element) {
                        //$state.go('main.login')
                        $state.go($state.current, {}, {reload: true});
                    }});
                } else if (data.status_code == -2) {
                    $scope.currentPasswordError = true;
                } else {
                    $scope.currentPasswordError = false;

                    $('#resetPasswordError').modal("show").modal({onApprove: function ($element) {
                        //$state.go('main.login')
                    }});
                }
            }).error(function (data, status) {
                $scope.changePasswordMsg = data.msg;

                $('#resetPasswordError').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            });
        }

        $scope.formSubmitted = false;
    }

    $scope.resetPassword = function () {
        $scope.formSubmitted = true;
        $scope.changePasswordForm.$submitted = true;

        $http.put('/api/users/' + $scope.userEdit.id, {
            user: $scope.userEdit,
            task: "resetPassword"
        }).success(function (data, status) {
            if (data.status_code == 1) {
                $('#resetPasswordSuccess').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            } else {
                $('#resetPasswordError').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            }
        }).error(function (data, status) {
            $('#resetPasswordError').modal("show").modal({onApprove: function ($element) {
                //$state.go('main.login')
            }});
        });

        $scope.formSubmitted = false;
    }

    $scope.deactivateAccount = function () {
        $scope.formSubmitted = true;
        $scope.changePasswordForm.$submitted = true;

        $http.put('/api/users/' + $scope.userEdit.id, {
            user: $scope.userEdit,
            task: "deactivateAccount"
        }).success(function (data, status) {
            if (data.status_code == 1) {
                $scope.userEdit = JSON.parse(data.user);
                $('#accountDeactivatedSuccess').modal("show").modal({onApprove: function ($element) {
                    if ($scope.userEdit.id == $rootScope.user.id) {
                        AuthService.logout();
                    } else {
                        $state.go('account.accountsTable');
                    }
                }});
            } else {
                $('#accountDeactivatedError').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            }
        }).error(function (data, status) {
            $('#accountDeactivatedError').modal("show").modal({onApprove: function ($element) {
                //$state.go('main.login')
            }});
        });

        $scope.formSubmitted = false;
    }

    $scope.activateAccount = function () {
        $scope.formSubmitted = true;
        $scope.changePasswordForm.$submitted = true;

        $http.put('/api/users/' + $scope.userEdit.id, {
            user: $scope.userEdit,
            task: "activateAccount"
        }).success(function (data, status) {
            if (data.status_code == 1) {
                $scope.userEdit = JSON.parse(data.user);

                $('#activateAccountSuccess').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            } else {
                $('#activateAccountError').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            }
        }).error(function (data, status) {
            $('#activateAccountError').modal("show").modal({onApprove: function ($element) {
                //$state.go('main.login')
            }});
        });

        $scope.formSubmitted = false;
    }

    $scope.toggleAdmin = function () {
        if ($scope.userEdit.role_id == 2) {
            $('#revokeAdminConfirmation').modal("show").modal({
                onApprove: function ($element) {
                    $http.put('/api/users/' + $scope.userEdit.id, {
                        user: $scope.userEdit,
                        task: "toggleAdmin"
                    }).success(function (data, status) {
                        $scope.userEdit = JSON.parse(data.user);

                        if ($rootScope.user.id == $scope.userEdit.id) {
                            $rootScope.user = $scope.userEdit;
                            sessionStorage.setItem("user", $scope.$rootScope.user);
                        }
                    }).error (function (data, status) {

                    });
                },
                onDeny: function(){
                    //return false;
                },
            });
        } else {
            $('#makeAdminConfirmation').modal("show").modal({
                onApprove: function ($element) {
                    $http.put('/api/users/' + $scope.userEdit.id, {
                        user: $scope.userEdit,
                        task: "toggleAdmin"
                    }).success(function (data, status) {
                        $scope.userEdit = JSON.parse(data.user);

                        if ($rootScope.user.id == $scope.userEdit.id) {
                            $rootScope.user = $scope.userEdit;
                        }
                    }).error (function (data, status) {

                    });
                },
                onDeny: function(){
                    //return false;
                },
            });
        }
    }

    $http.get('/api/products').success(function (data, status) {
        $scope.products = data;
    });

    $scope.removeProduct = function (index) {
        if ($scope.new_products_user.length > 1) {
            $scope.new_products_user.splice(index, 1);
        }
    }

    $scope.deleteProduct = function (index, product) {
        $http.put('/api/users/' + $scope.userEdit.id, {
            product: product,
            task: "deleteProduct"
        }).success(function (data, status) {
            $scope.products_user.splice(index);
        }).error(function (data, status) {

        });
    }

    $scope.updateProductsUser = function () {
        $http.put('/api/users/' + $scope.userEdit.id, {
            products: $scope.new_products_user,
            task: "addProducts"
        }).success(function (data, status) {
            $scope.updateProductMsg = data.msg;
            $scope.products_user = [];
            $scope.products_user = data.products_user;
            $scope.new_products_user = [];
            $scope.new_products_user.push({});

            $('#productUpdateModal').modal("show").modal({onApprove: function ($element) {

            }});
        }).error(function (data, status) {

        });
    }
});
