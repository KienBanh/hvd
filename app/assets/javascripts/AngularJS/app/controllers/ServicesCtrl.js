var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('ServicesCtrl', function ($scope, $http, $timeout, $stateParams, $location) {
    'use strict';

    //$scope.$on('$viewContentLoaded', function () {
    //    if ($stateParams.tabName) {
    //        $scope.tabName = $stateParams.tabName;
    //    } else {
    //        $scope.tabName = "testing";
    //    }
    //    ////
    //    //$('.slider-for-testing').slick({
    //    //    slidesToShow: 1,
    //    //    slidesToScroll: 1,
    //    //    arrows: false,
    //    //    fade: true,
    //    //    asNavFor: '.slider-nav-testing',
    //    //    autoplay: true,
    //    //    dots: true
    //    //});
    //
    //    //$('.slider-nav-testing').slick({
    //    //    slidesToShow: 3,
    //    //    slidesToScroll: 1,
    //    //    asNavFor: '.slider-for-testing',
    //    //    centerMode: true,
    //    //    focusOnSelect: true,
    //    //    arrows: false,
    //    //    dots: true
    //    //});
    //
    //    //$('.slider-for-training').slick({
    //    //    slidesToShow: 1,
    //    //    slidesToScroll: 1,
    //    //    arrows: false,
    //    //    fade: true,
    //    //    asNavFor: '.slider-nav-training',
    //    //    autoplay: true,
    //    //    dots: true
    //    //});
    //
    //    //$('.slider-nav-training').slick({
    //    //    slidesToShow: 4,
    //    //    slidesToScroll: 1,
    //    //    asNavFor: '.slider-for-training',
    //    //    centerMode: true,
    //    //    focusOnSelect: true,
    //    //    arrows: false,
    //    //    dots: true
    //    //});
    //
    //    $http.get("/api/files_in_directory", {params: {path: "Services/Testing"}}).success(function (data, status) {
    //        $scope.testing = data;
    //    }).then(function () {
    //        //if ($scope.testing) {
    //        //    angular.forEach($scope.testing, function (value, key) {
    //        //        $('.slider-for-testing').slick('slickAdd', '<div><img src="' + value + '" class="ui centered align image" /></div>');
    //        //        //$('.slider-nav-testing').slick('slickAdd', '<div><img src="' + value + '" class="ui centered align small image" /></div>');
    //        //    });
    //        //}
    //        $timeout(function () {
    //            $('.slider-for-testing').slick({
    //                dots: true,
    //                infinite: true,
    //                speed: 300,
    //                slidesToShow: 1,
    //                centerMode: true,
    //                //variableWidth: true,
    //                autoplay: true,
    //                arrows: false
    //            });
    //        }, 0);
    //
    //    });
    //
    //    $http.get("/api/files_in_directory", {params: {path: "Services/Training"}}).success(function (data, status) {
    //        $scope.training = data;
    //    }).then(function () {
    //        //if ($scope.training) {
    //        //    angular.forEach( $scope.training, function (value, key) {
    //        //        $('.slider-for-training').slick('slickAdd', '<div><img src="' + value + '" class="ui centered align image" /></div>');
    //        //        //$('.slider-nav-training').slick('slickAdd', '<div><img src="' + value + '"  class="ui centered align small image" /></div>');
    //        //    });
    //        //}
    //
    //        $timeout(function () {
    //            $('.slider-for-training').slick({
    //                dots: true,
    //                infinite: true,
    //                speed: 300,
    //                slidesToShow: 1,
    //                centerMode: true,
    //                //variableWidth: true,
    //                autoplay: true,
    //                arrows: false
    //            });
    //        }, 1000);
    //    });
    //});
    //
    //$('.menu .item').tab({
    //    history: true,
    //    historyType: 'state',
    //    path: '/services/',
    //    onFirstLoad: function (tabPath, parameterArray, historyEvent) {
    //        if (tabPath === "testing") {
    //
    //        }
    //
    //        if (tabPath == "training") {
    //
    //        }
    //    },
    //    onLoad: function (tabPath, parameterArray, historyEvent) {
    //        $scope.tabName = tabPath;
    //        //$location.path("services/"+tabPath).replace();
    //    }
    //});


    $scope.$on('$viewContentLoaded', function () {
        if ($stateParams.tabName) {
            $scope.tabName = $stateParams.tabName;

            $timeout(function () {
                // $('#' +  $scope.tabName).slick({
                //     slidesToShow: 1,
                //     slidesToScroll: 1,
                //     //asNavFor: '.slider-for-testing',
                //     centerMode: true,
                //     focusOnSelect: true,
                //     arrows: false,
                //     dots: true
                // });

            $('#' +  $scope.tabName + '-for').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              //arrows: false,
              fade: true,
              asNavFor: '.slider-nav',
            });
            $('#' +  $scope.tabName + '-nav').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '#' +  $scope.tabName + '-for',
              dots: true,
              centerMode: true,
              focusOnSelect: true,
              autoplay: true
            });
            }, 1000);
        } else {
            $scope.tabName = "testing";

            $timeout(function () {
                // $('#' +  $scope.tabName).slick({
                //     slidesToShow: 1,
                //     slidesToScroll: 1,
                //     //asNavFor: '.slider-for-testing',
                //     centerMode: true,
                //     focusOnSelect: true,
                //     arrows: false,
                //     dots: true
                // });

            $('#' +  $scope.tabName + '-for').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              //arrows: false,
              fade: true,
              asNavFor: '.slider-nav'
            });
            $('#' +  $scope.tabName + '-nav').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '#' +  $scope.tabName + '-for',
              dots: true,
              centerMode: true,
              focusOnSelect: true,
              autoplay: true
            });
            }, 1000);
        }
    });

    $http.get("/api/files_in_directory", {params: {path: "Services/Testing"}}).success(function (data, status) {
        $scope.testing = data;
    });
    $http.get("/api/files_in_directory", {params: {path: "Services/Training"}}).success(function (data, status) {
        $scope.training = data;
    });

    $('.menu .item').tab({
        onFirstLoad: function (tabPath, parameterArray, historyEvent) {
            $timeout(function () {
                // $('#' + tabPath).slick({
                //     slidesToShow: 1,
                //     slidesToScroll: 1,
                //     //asNavFor: '.slider-for-testing',
                //     centerMode: true,
                //     focusOnSelect: true,
                //     arrows: false,
                //     dots: true
                // });

            $('#' +  tabPath + '-for').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              //arrows: false,
              fade: true,
              asNavFor: '.slider-nav'
            });
            $('#' +  tabPath + '-nav').slick({
              slidesToShow: 3,
              slidesToScroll: 1,
              asNavFor: '#' +  tabPath + '-for',
              dots: true,
              centerMode: true,
              focusOnSelect: true,
              autoplay: true
            });
            }, 0);
        },
        onRequest: function (tabPath) {

        },
        onLoad: function (tabPath, parameterArray, historyEvent) {

        },
        onVisible: function (tabPath) {

        }

    });

});