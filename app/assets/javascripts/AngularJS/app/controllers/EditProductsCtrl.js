var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('EditProductsCtrl', function($scope, $rootScope, $http, $timeout, categories, DataSheets) {
    "use strict";

    $rootScope.$on("$viewContentLoading", function() {});

    $http.get("/api/products").success(function(data, status) {
        $scope.products = data.filter(function(product) {
            return product.isCustom === false;
        });;
    });

    $scope.getCategoriesName = function(id) {
        var category = _.findWhere(categories.data, { id: parseInt(id) })

        if (category) {
            return category.name;
        } else {
            return "Other"
        }
    }

    $scope.dataSheets = DataSheets.get();

    $scope.selectFile = function(product) {
        $('.selectFileModal').modal('show');

        $scope.productToEdit = product;
    }

    $scope.updateFile = function(filename) {
        $http.put("/api/products/" + $scope.productToEdit.id, { data_sheet: filename }).success(function(data, status) {
            console.log(data)

            if (data.status == 1) {
                $scope.products = data.products;

                $('.selectFileModal').modal('hide');
            } else {
                alert(data.msg);

                $('.selectFileModal').modal('hide');
            }
        }).error(function(data, status) {
            alert(data.msg);

            $('.selectFileModal').modal('hide');
        })
    }
});