var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('EditRmaController', function ($scope, $rootScope, $timeout, $http, $filter, $state, $stateParams, FileUploader) {
    $http.get('/api/statuses').success(function (data, status) {
        $scope.statuses = data;
    });

    $http.get('/api/products').success(function (data, status) {
        $scope.products = data;
    });

    $http.get("/api/rmas/" + $stateParams.id).success(function (data, status) {
        if (data.status == 1) {
            $scope.rma = data.rma;
            $scope.attachments = data.attachments;
            $scope.receivedParts = data.received_parts;
            $scope.messages = data.rmaMessages;
        } else {
            $scope.rma = {};
            $scope.attachments = [];
            alert(data.msg);
        }
    }).error(function (data, status) {
        alert(data.msg);
    });

    $scope.totalRepair = function () {
        return "0"
    }

    $scope.subtotal = function () {
        if ($scope.rma) {
            if (parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.parts_labor_cost)) {
                return parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.parts_labor_cost);
            } else {
                return
            }
        }
    }

    $scope.total = function () {
        if ($scope.rma) {
            if (parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.calibration_cost) + parseFloat($scope.rma.parts_labor_cost)) {
                return parseFloat($scope.rma.hours_spent * $scope.rma.rate) + parseFloat($scope.rma.calibration_cost) + parseFloat($scope.rma.parts_labor_cost)
            } else {
                return
            }
        }
    }

    $scope.uploader = new FileUploader({
            url: "/api/rmas/public_upload",
            headers : {
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
            formData: [
                {id: $stateParams.id}
            ],
            removeAfterUpload: true,
            onCompleteItem: function (item, response, status, headers) {
                $("#addPublicAttachmentInput").value = "";
                $(".publicAttachmentButton").removeClass("loading").removeClass("disabled");

                if (response.status == 1) {
                    $scope.attachments = response.attachments;
                    return;
                } else {
                    alert(response.msg);
                    return;
                }

                if (response.msg) {
                    alert(response.msg);
                }

                if (status == 413) {
                    alert("The file you are uploading is too large.");
                }
            },
            queueLimit: 1,
            onBeforeUploadItem: function (item) {
                $(".publicAttachmentButton").addClass("loading").addClass("disabled");

                if (item.file.size > 5000000) {
                    return false;
                }

                if ($scope.attachments.length >= 12) {
                    return false;
                }
            }
        }
    );

    $scope.uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
});