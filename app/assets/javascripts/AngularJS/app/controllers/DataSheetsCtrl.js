'use strict';

var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('DataSheetsCtrl', function($rootScope, $scope, DataSheets) {
    $scope.dataSheets = DataSheets.get();
});