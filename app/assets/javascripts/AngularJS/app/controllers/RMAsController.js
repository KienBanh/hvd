var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('RMAsController', function($scope, $rootScope, $timeout, $http, $filter) {
    $http.get('/api/products').success(function(data, status) {
        $scope.products = data;
    });

    $http.get('/api/statuses').success(function(data, status) {
        $scope.statuses = data;
    });

    $http.get("/api/rmas/").success(function(data, status) {
        if (data.status == 1) {
            $scope.rmas = data.rmas;
        } else {
            $scope.rmas = [];
        }
    }).error(function(data, status) {
        console.log(status, data);
    });
});

hvdiagnostics.directive("rmasTableRepeat", function($timeout) {
    return function(scope, element, attrs) {
        if (scope.$last) {
            $timeout(function() {
                var table = $("#rmaSummaryTable").DataTable({
                    'order': [
                        [0, 'desc']
                    ]
                });

                table.columns().every(function() {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function() {
                        if (that.search() !== this.value) {
                            that
                                .search(this.value)
                                .draw();
                        }
                    });
                });
            }, 0);
        }
    }
});