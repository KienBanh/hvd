var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('RootCtrl', function ($scope, $rootScope, $timeout, $state, $http, $location, $window) {
    // $scope.$on('$viewContentLoaded', function () {
    //     $('.dropdown').dropdown();

    //     $('.ui.search').search({
    //         source: content,
    //         onSelect: function (result, response) {
    //            $window.location.href = result.url;
    //         },
    //         searchFields: [
    //             'title',
    //             'tag'
    //         ],
    //        // type: 'category'
    //     });
    // })

    // var content = [
    //     { title: 'HVA28', tag: 'VLF/DC Hipots', url: '/products/hva#HVA28' },
    //     { title: 'HVA28TD', tag: 'VLF/DC Hipots', url: '/products/hva#HVA28TD' },
    //     { title: 'HVA34', tag: 'VLF/DC Hipots', url: '/products/hva#HVA34' },
    //     { title: 'HVA 30-5', tag: ' VLF/DC Hipots', url: '/products/hva#HVA 30-5' },
    //     { title: 'HVA 60', tag: ' VLF/DC Hipots', url: '/products/hva#HVA 60' },
    //     { title: 'HVA 90/94', tag: ' VLF/DC Hipots', url: '/products/hva#HVA 90/94' },
    //     { title: 'TD30', tag: 'Tan Delta Cable Diagnostics', url: '/products/td#TD30' },
    //     { title: 'TD60', tag: 'Tan Delta Cable Diagnostics', url: '/products/td#TD60' },
    //     { title: 'TD90', tag: 'Tan Delta Cable Diagnostics', url: '/products/td#TD90'},
    //     { title: 'EZ-Cable ID', tag: 'Cable ID & Phasing', url: '/products/cableID#EZ-Cable ID' },
    //     { title: '1205 CXA TDR', tag: 'Time Domain Reflectometry (TDR)', url: '/products/tdr#1205 CXA TDR' },
    //     { title: 'BA75', tag: 'Dielectric Oil Testing', url: '/products/ba#BA75'},
    //     { title: 'BA100', tag: 'Dielectric Oil Testing', url: '/products/injectors#BA100'},
    //     { title: '100 ADM-MK4', tag: 'Current Injectors', url: '/products/injectors#100 ADM-MK4'},
    //     { title: '750ADM mk2 / 750ADM-H mk2', tag: 'Current Injectors', url: '/products/injectors#750ADM mk2 / 750ADM-H mk2'},
    //     { title: 'DSM200', tag: 'Micro-Ohmmeters', url: '/products/micro-ohm#DSM200'},
    //     { title: 'DMO200', tag: 'Micro-Ohmmeters', url: '/products/micro-ohm#DMO200'},
    //     { title: 'DSM600', tag: 'Micro-Ohmmeters', url: '/products/micro-ohm#DSM600'}
    // ];

    $scope.logout = function () {
        delete $rootScope.user;

        $state.go('main.login', {reload: true});
    }
});