var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('AuthCtrl', function ($scope, $rootScope, $timeout, $state, $http, $location, $window) {
    $scope.loginSubmit = function () {
        $scope.login.validations = false;
        $scope.login.email_address = $scope.login.email_address.trim();
        $scope.login.password = $scope.login.password.trim();

        $http.post('/api/sessions/login', $scope.login).success(function (data, status) {
            if (data == -1) {
                $scope.loginStatus = data;
                return
            }

            $rootScope.user = data;

            sessionStorage.setItem('user', JSON.stringify(data));

            if ($rootScope.user.password_expires_on) {
                alert("Your password expired. You must change your password now.");

                $state.go('account.edit', {id: $rootScope.user.id});
            } else {
                $state.go('account');
            }
        }).error(function (data, status) {
            $scope.loginStatus = data;
        });
    }

    $scope.resetPassword = function () {
        if (!$scope.login.email_address) {
            requireEmail
            $('#requireEmail').modal("show").modal({onApprove: function ($element) {
                //$state.go('main.login')
            }});

            return false;
        }

        $http.post('/api/sessions/reset_password', $scope.login).success(function (data, status) {
            if (data.status_code == 1) {
                $('#resetPasswordSuccess').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            } else {
                $('#resetPasswordError').modal("show").modal({onApprove: function ($element) {
                    //$state.go('main.login')
                }});
            }
        }).error(function (data, status) {
            $('#resetPasswordError').modal("show").modal({onApprove: function ($element) {
                //$state.go('main.login')
            }});
        });

    }
});