var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('NewRmaController', function($scope, $rootScope, $timeout, $state, $http) {
    "use strict";

    // scope
    $scope.accept = false;

    $http.get('/api/products').success(function(data, status) {
        $scope.products = data.filter(function(product) {
            return product.isCustom === false;
        });
        $scope.products.push({ id: 0, category_id: "", name: "Other" });
    });

    Dropzone.autoDiscover = false;

    var myDropzone = new Dropzone("div#dropzone", {
        method: "post",
        url: "/api/files/upload",
        addRemoveLinks: true,
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        maxFilesize: 25,
        maxFiles: 12,
        acceptedFiles: "image/*, video/*, application/pdf"
    });

    myDropzone.on("success", function(file, response) {
        $scope.rma.attachments.push(response);
    });

    myDropzone.on("error", function(file, errorMessage) {

    });

    myDropzone.on("removedfile", function(file) {
        var attachment = _.findWhere($scope.rma.attachments, { fileName: file.name });

        $scope.rma.attachments = _.without($scope.rma.attachments, attachment);

        $http.post("/api/files/delete", attachment).success(function(data, status) {}).error(function(data, status) {});
    });

    $scope.rma = {};
    $scope.rma.attachments = [];

    if ($rootScope.user.role_id != 2) {
        $scope.rma.user_id = $rootScope.user.id;
        $scope.rma.first_name = $rootScope.user.first_name;
        $scope.rma.last_name = $rootScope.user.last_name;
        $scope.rma.email_address = $rootScope.user.email_address;
        $scope.rma.company_name = $rootScope.user.company_name;
        $scope.rma.phone_number = $rootScope.user.phone_number;
        $scope.rma.shipping_address1 = $rootScope.user.street_address1;
        $scope.rma.shipping_address2 = $rootScope.user.street_address2;
        $scope.rma.shipping_city = $rootScope.user.city;
        $scope.rma.shipping_state = $rootScope.user.state;
        $scope.rma.shipping_country = $rootScope.user.country;
        $scope.rma.shipping_postal_code = $rootScope.user.postal_code;
        $scope.rma.billing_address1 = $rootScope.user.street_address1;
        $scope.rma.billing_address2 = $rootScope.user.street_address2;
        $scope.rma.billing_city = $rootScope.user.city;
        $scope.rma.billing_state = $rootScope.user.state;
        $scope.rma.billing_country = $rootScope.user.country;
    } else {
        $http.get('/api/users').success(function(data, status) {
            $scope.users = data;
        }).then(function() {

            $('.ui.search')
                .search({
                    source: $scope.users,
                    maxResults: 1000,
                    searchFields: [
                        'first_name',
                        'last_name',
                        'email_address',
                        'company_name'
                    ],
                    searchFullText: false,
                    onSelect: function(result, response) {
                        var user = _.findWhere($scope.users, { id: result.id });
                        $scope.rma.user_id = user.id;
                        $scope.rma.first_name = user.first_name;
                        $scope.rma.last_name = user.last_name;
                        $scope.rma.email_address = user.email_address;
                        $scope.rma.company_name = user.company_name;
                        $scope.rma.phone_number = user.phone_number;
                        $scope.rma.shipping_address1 = user.street_address1;
                        $scope.rma.shipping_address2 = user.street_address2;
                        $scope.rma.shipping_city = user.city;
                        $scope.rma.shipping_state = user.state;
                        $scope.rma.shipping_country = user.country;
                        $scope.rma.shipping_postal_code = user.postal_code;
                        $scope.rma.billing_address1 = user.street_address1;
                        $scope.rma.billing_address2 = user.street_address2;
                        $scope.rma.billing_city = user.city;
                        $scope.rma.billing_state = user.state;
                        $scope.rma.billing_country = user.country;
                        $scope.rma.billing_postal_code = user.postal_code;
                        $scope.$apply();
                    },
                    fields: {
                        title: 'first_name',
                        description: "email_address"
                    }
                });
        });
    }

    $scope.$on("$viewContentLoaded", function() {
        //$('.dropdown').dropdown();
    });

    $('.calibrationRequiredCheckbox').checkbox({
        onChecked: function() {
            $timeout(function() {
                $scope.rma.calibration_required = true;
            }, 0);
        },
        onUnchecked: function() {
            $timeout(function() {
                $scope.rma.calibration_required = false;
            }, 0);
        }
    });

    $('.insuranceRequiredCheckbox').checkbox({
        onChecked: function() {
            $timeout(function() {
                $scope.rma.shipping_insurance_required = true;
            }, 0);
        },
        onUnchecked: function() {
            $timeout(function() {
                $scope.rma.shipping_insurance_required = false;
            }, 0);
        }
    });

    $scope.submitNewRmaForm = function() {
        $scope.rma.billing_address1 = $scope.rma.shipping_address1;
        $scope.rma.billing_address2 = $scope.rma.shipping_address2;
        $scope.rma.billing_city = $scope.rma.shipping_city;
        $scope.rma.billing_state = $scope.rma.shipping_state;
        $scope.rma.billing_country = $scope.rma.shipping_country;
        $scope.rma.billing_postal_code = $scope.rma.shipping_postal_code;

        if ($scope.rma.product_id > 0) {
            $scope.rma.product_name = _.findWhere($scope.products, { id: $scope.rma.product_id }).name;
        } else {
            $scope.rma.isCustom = true;
        }

        $http.post("/api/rmas", $scope.rma).success(function(data, status) {
            if (data.status == 1) {
                $scope.rma_number = data.rma.rma_number;
                $scope.rma_id = data.rma.id;

                $('.rmaResultModal')
                    .modal({
                        blurring: true,
                        closable: false,
                        dimmerSettings: {
                            closable: false
                        },
                        onHidden: function() {
                            if ($rootScope.user.role_id == 2) {
                                $state.go("account.manageRmas");
                            } else {
                                $state.go("account.rmas");
                            }
                        }
                    })
                    .modal('show');


            } else {
                alert(data.msg);
            }
        }).error(function(data, status) {
            alert(data.msg);
        });
    }

    $scope.shippingCountryChange = function() {
        if ($scope.rma.shipping_country === 'CA') {
            $scope.rma.shipping_state = "";
        }
    }
});