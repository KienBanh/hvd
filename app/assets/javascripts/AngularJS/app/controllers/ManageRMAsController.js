var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('ManageRMAsController', function($scope, $rootScope, $timeout, $http, $filter) {
    // $http.get('/api/products').success(function (data, status) {
    //     $scope.products = data;
    // });

    $http.get('/api/statuses').success(function(data, status) {
        $scope.statuses = data;
    }).then(function() {
        var datatable = $('#rmaSummaryTable').DataTable({
            ajax: {
                url: '/api/rmas/',
                type: 'GET',
                dataSrc: function(json) {
                    return json.rmas;
                }
            },
            columns: [
                { data: 'rma_number' },
                {
                    data: 'updated_at',
                    render: function(data, type, full, meta) {
                        return $filter('date')(data, 'shortDate');
                    }
                },
                {
                    data: 'created_at',
                    render: function(data, type, full, meta) {
                        return $filter('date')(data, 'shortDate');
                    }
                },
                { data: 'product_name' },
                { data: 'serial_number' },
                { data: 'first_name' },
                { data: 'last_name' },
                { data: 'company_name' },
                { data: 'email_address' },
                { data: 'phone_number' },
                { data: 'description' }, {
                    data: 'status_id',
                    render: function(data, type, full, meta) {
                        return _.findWhere($scope.statuses, { id: data }).name;;
                    }
                }, {
                    data: 'id',
                    render: function(data, type, full, meta) {
                        return '<a class="ui green icon button" href="/account/admin/rma/edit/' + data + '" target="_self" title="Edit"><i class="edit icon"></i></a>';
                    },
                    className: 'center aligned'
                }
            ],
            order: [
                [0, 'desc']
            ]
        });

        datatable.columns().every(function() {
            var that = this;

            $('input', this.footer()).on('keyup change', function() {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
    });

    // $('#rmaSummaryTable').DataTable({
    //     ajax: {
    //         url: '/api/rmas/',
    //         type: 'GET',
    //         dataSrc: function(json) {
    //             return json.rmas;
    //         }
    //     },
    //     columns: [
    //         { data: 'rma_number' }, {
    //             data: 'created_at',
    //             render: function(data, type, full, meta) {
    //                 return $filter('date')(data, 'shortDate');
    //             }
    //         }, { data: 'product_name' },
    //         { data: 'serial_number' },
    //         { data: 'first_name' },
    //         { data: 'last_name' },
    //         { data: 'company_name' },
    //         { data: 'email_address' },
    //         { data: 'phone_number' },
    //         { data: 'description' },
    //         { 
    //             data: 'status_id',
    //             render: function(data, type, full, meta) {
    //                 return _.findWhere($scope.statuses, {id: data}).name;;
    //             }
    //          }, {
    //             data: 'id',
    //             render: function(data, type, full, meta) {
    //                 return '<a class="ui green icon button" href="/account/admin/rma/edit/' + data +'" target="_self" title="Edit"><i class="edit icon"></i></a>';
    //             },
    //             className: 'center aligned'
    //         }
    //     ]
    // });

    // $http.get("/api/rmas/").success(function(data, status) {
    //     if (data.status == 1) {
    //         $scope.rmas = data.rmas;
    //     } else {
    //         $scope.rmas = [];
    //     }
    // }).error(function(data, status) {});
});