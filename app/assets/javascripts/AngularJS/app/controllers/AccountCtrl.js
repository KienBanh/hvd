var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.controller('AccountCtrl', function ($scope, $rootScope, $timeout, $state, $http, $location, $window, AuthService) {
    'use strict';

    //$scope.$on("$viewContentLoaded", function () {
    //    $('.ui.sidebar')
    //        .sidebar({
    //            context: $('.bottom.segment'),
    //            //closable: false,
    //            //dimPage: false,
    //            //transition: "overlay"
    //        })
    //        .sidebar('attach events', '.toggleSidebar');
    //
    //    //$('.accountSidebar').first().sidebar('attach events', '.toggleSidebar');
    //
    //    //$('.ui.accordion').accordion();
    //});

    $scope.logout = function () {
        AuthService.logout();
    }
});