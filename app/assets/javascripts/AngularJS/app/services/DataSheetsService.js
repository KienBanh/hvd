var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.factory("DataSheets", function($resource) {
    return $resource('/api/data_sheets/:id');
});