var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.factory('FaqsService', function ($resource) {
    'use strict';

    return $resource('/api/faqs/:id');
});