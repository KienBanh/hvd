var hvdiagnostics = angular.module('hvdiagnostics');

hvdiagnostics.service('AuthService', function ($rootScope, $state) {
    'use strict';

    this.login = function (credentials) {
        return $http.post('/api/sessions/login', credentials);
    }

    this.logout = function () {
        delete $rootScope.user;

        sessionStorage.clear();

        $state.go('main.login', {reload: true});
    }
});