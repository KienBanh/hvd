angular.module('hvdiagnostics').filter('filename', function() {
    return function(input) {
        var result;
        try{
            result = input.substring(input.lastIndexOf("/"));
            result = result.replace('/', '');
            result = result.replace('_', '/');
        } catch (e) {
            result = input;
        }

        return result;
    };
}).filter('productNameFromProductId', function() {
    return function(input, products) {
        if (products) {
            return _.findWhere(products, {id: input}).name;
        }
    };
}).filter('statusNameFromStatusId', function() {
    return function(input, statuses) {
        if (statuses) {
            if (_.findWhere(statuses, {id: input})) {
                return _.findWhere(statuses, {id: input}).name;
            }
        }
    };
}).filter('userNameFromId', function($http) {
    return function(input) {
        $http.get("/api/users/user_name/" + input).success(function (data, status) {
            return data
        }).error(function () {
            return "Unknown"
        })
    };
});
