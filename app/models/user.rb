class User < ActiveRecord::Base
  has_many :products, through: :products_users
  has_many :products_users
  has_many :rmas, through: :rmas_users
  has_many :rmas_users
  has_many :messages

  has_secure_password
end