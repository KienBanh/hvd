class Rma < ActiveRecord::Base
  has_many :users, through: :rmas_users
  has_many :rmas_users
  has_many :attachments
  has_many :received_parts
  has_many :messages
  has_many :used_parts
end
