class Faq < ActiveRecord::Base
  before_save :set_order_attribute

  def set_order_attribute
    current_maximum = Faq.maximum("faq_order")

    if current_maximum
      self.faq_order = current_maximum + 1 if self.faq_order.nil?
    else
      self.faq_order = 1
    end
  end
end