class Api::CategoriesController < ApplicationController
  def index
    @categories = Category.all.where.not(id: 11)

    render :json => @categories
  end
end