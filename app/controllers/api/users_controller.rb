require 'postmark'
require 'json'
class Api::UsersController < ApplicationController
  #before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user, only: [:index, :show, :edit, :update, :destroy]
  before_action :is_admin, only: [:index, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.where(deleted: false)

    render :json => @users.to_json(:except => [:password, :password_digest])
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    products_users = @user.products_users

    render :json => {:user => @user.to_json(:except => [:password, :password_digest]), :products_user => products_users}
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit

  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @user.email_address = @user.email_address.downcase.strip
    @existingUser = User.find_by_email_address(user_params[:email_address])

    # auto activate the user if another in the same domain is already registered and active
    if User.where("email_address LIKE ? AND active = ?", "%#{user_params["email_address"].split("@")[1]}", "true").count > 0
      @user.active = true
    else
      @user.active = false
    end

    begin
      logger.debug @user

      if @existingUser == nil && @user.save!


        params["products"].each do |product|
          if product["id"] === "other"
            newProduct = Product.new({category_id: 11, name: product["name"], serial_number: product["serial_number"]})
            newProduct.save

            @user.products_users.build({product_id: newProduct.id}).save        
          else
            @user.products_users.build({product_id: product["id"], serial_number: product["serial_number"]}).save
          end
        end

        client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)
        newAccountEmailToAdmin = client.deliver(
            from: 'salesdistributionlist@hvdiagnostics.com',
            to: 'salesdistributionlist@hvdiagnostics.com',
            subject: 'New Account Created',
            html_body: "<div>A new account has been created.</div>",
            track_opens: true
        )

        newAccountEmailToClient = client.deliver_with_template(
            {:from=>"salesdistributionlist@hvdiagnostics.com",
             :to=>"#{@user.email_address}",
             :template_id=>75642,
             :template_model=>
                 {"product_name"=>"HV Diagnostics",
                  "name"=>"#{@user.first_name}",
                  "username"=>"#{@user.email_address}",
                  "product_address_line1"=>"271 Rope Mill Parkway, Suite #2",
                  "product_address_line2"=>"Woodstock, GA 30188 USA",
                  "product_phone_number"=>"+1 (678) 445-2555",
                  "sender_name"=>"salesdistributionlist@hvdiagnostics.com"}
            }
        )

        if User.where("email_address LIKE ? AND active = ?", "%#{user_params["email_address"].split("@")[1]}", "true").count > 0
          if (newAccountEmailToClient["error_code"] == 0)
            @user.active = true

            if (@user.save)
              client.deliver_with_template(
                  {:from=>"salesdistributionlist@hvdiagnostics.com",
                   :to=>"#{@user.email_address}",
                   :template_id=>75743,
                   :template_model=>
                       {"product_name"=>"HV Diagnostics",
                        "name"=>"#{@user.first_name}",
                        "username"=>"#{@user.email_address}",
                        "product_address_line1"=>"271 Rope Mill Parkway, Suite #2",
                        "product_address_line2"=>"Woodstock, GA 30188 USA",
                        "product_phone_number"=>"+1 (678) 445-2555",
                        "sender_name"=>"salesdistributionlist@hvdiagnostics.com"}
                  }
              )
            end
          end
        end

        render :json => @user.to_json(:only => :active)
      elsif @existingUser != nil
        render :json => -2
      else
        render :json => -1
      end
    rescue Exception => e
      logger.error e
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @request = User.find(params[:id])
    @sessionUser = User.find(session[:user_id])

    if session[:user_id] != @request.id && @sessionUser.role_id != 2
      render :json => {:status_code => 0, :error_msg => "No changes were made."}

      return false
    end

    case params[:task]
      when "updateAccount"
        @request.assign_attributes(user_params)

        if @request.changed?
          @request.updated_at = DateTime.now

          if @request.save
            render :json => {:status_code => 1, :msg => "Account successfully updated."}
          else
            render :json => {:status_code => -1, :msg => "Something went wrong. No changes were made."}
          end
        else
          render :json => {:status_code => 0, :msg => "No changes were made."}
        end
      when "updateEmailAddress"
        @existingUser = User.find_by_email_address(params[:newEmailAddress])

        if @existingUser
          render :json => {:status_code => -1, :error_msg => "This email address is already registered in our system. Please pick another email address."}
        elsif !@existingUser
          @request.email_address = params[:newEmailAddress].downcase.strip
          @request.updated_at = DateTime.now

          if @request.save
            render :json => {:status_code => 1, :msg => "Email successfully updated."}
          else
            render :json => {:status_code => -2, :msg => "Something went wrong. No changes were made."}
          end
        else
          render :json => {:status_code => 0, :msg => "No changes were made."}
        end
      when "changePassword"
        if @request.authenticate(params[:newPassword][:currentPassword])
          @request.password = params[:newPassword][:password]
          @request.password_expires_on = nil
          @request.updated_at = DateTime.now

          if @request.save
            render :json => {:status_code => 1, :msg => "Password was successfully changed."}
          else
            render :json => {:status_code => -1, :msg => "Something went wrong. Password was not successfully changed."}
          end
        else
          render :json => {:status_code => -2, :msg => "Current password does not match what is in the system."}
        end
      when "deactivateAccount"
          @request.deleted = true
          @request.updated_at = DateTime.now

          if @sessionUser.role_id === 2 && @request.save
            render :json => {:status_code => 1, :msg => "Account was successfully deleted.", :user => @request.to_json(:except => [:password, :password_digest])}
          else
            render :json => {:status_code => -1, :msg => "Something went wrong. No changes were made."}
          end      
      when "activateAccount"
        if !@request.active
          @request.active = true
          @request.updated_at = DateTime.now

          if @sessionUser.role_id === 2 && @request.save
            render :json => {:status_code => 1, :msg => "Account was successfully activated.", :user => @request.to_json(:except => [:password, :password_digest])}

            client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)
            client.deliver_with_template(
                {:from=>"salesdistributionlist@hvdiagnostics.com",
                 :to=>"#{@request.email_address}",
                 :template_id=>75743,
                 :template_model=>
                     {"product_name"=>"HV Diagnostics",
                      "name"=>"#{@request.first_name}",
                      "username"=>"#{@request.email_address}",
                      "product_address_line1"=>"271 Rope Mill Parkway, Suite #2",
                      "product_address_line2"=>"Woodstock, GA 30188 USA",
                      "product_phone_number"=>"+1 (678) 445-2555",
                      "sender_name"=>"salesdistributionlist@hvdiagnostics.com"}
                }
            )
          end
        else
          render :json => {:status_code => 0, :msg => "Something went wrong. Account was not successfully activated."}
        end
      when "resetPassword"
        random_password = SecureRandom.hex 3

        @request.password = random_password
        @request.password_expires_on = DateTime.now
        @request.updated_at = DateTime.now

        if @request.save
          render :json => {:status_code => 1, :msg => "Password reset was successful."}

          client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)
          client.deliver_with_template(
              {:from=>"salesdistributionlist@hvdiagnostics.com",
               :to=>"#{@request.email_address}",
               :template_id=>108102,
               :template_model=>
                   {"product_name"=>"HV Diagnostics",
                    "name"=>"#{@request.first_name}",
                    "temporary_password"=>"#{random_password}",
                    "product_address_line1"=>"271 Rope Mill Parkway, Suite #2",
                    "product_address_line2"=>"Woodstock, GA 30188 USA",
                    "product_phone_number"=>"+1 (678) 445-2555"}
              }
          )
        else
          render :json => {:status_code => -1, :msg => "Something went wrong. Password reset was not successful."}
        end
      when "toggleAdmin"
        if @request.role_id != 2
          @request.role_id = 2
        else
          @request.role_id = 1
        end

        if @sessionUser.role_id === 2 && @request.save && @request.role_id == 2
          render :json => {:status_code => 1, :msg => "This user is now an administrator.", :user => @request.to_json(:except => [:password, :password_digest])}
        elsif @sessionUser.role_id === 2 && @request.save && @request.role_id != 2
          render :json => {:status_code => 1, :msg => "This user is no longer an administrator.", :user => @request.to_json(:except => [:password, :password_digest])}
        else
          render :json => {:status_code => -1, :msg => "Something went wrong. No changes were made"}
        end
      when "deleteProduct"
        ProductsUser.delete_all(["product_id = ? AND user_id = ? AND serial_number = ?", params[:product][:product_id], params[:product][:user_id], params[:product][:serial_number]])

        products_users = @request.products_users

        render :json => {:status_code => 1, :msg => "Product was succesfully deleted.", :products_user => products_users}
      when "addProducts"
        logger.info params

        params[:products].each do |product|
          logger.info product
          #existingProduct = @request.products_users.where("product_id = ? AND serial_number = ? AND user_id = 1", product[:product_id], product[:serial_number])

          if !@request.products_users.exists?(['product_id = ? AND serial_number = ?', product[:product_id], product[:serial_number]])
            if @request.products_users.build({product_id: product["product_id"], serial_number: product["serial_number"]}).save
            else
              render :json => {:status_code => 1, :msg => "Something went wrong. Product was not succesfully updated.", :products_user => @request.products_users}
            end
          end
        end

        render :json => {:status_code => 1, :msg => "Product was succesfully updated.", :products_user => @request.products_users}
      else
        render :json => {:status_code => 0, :msg => "No changes were made."}
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def user_name
    logger.info params
    @user = User.find_by_id(params[:id])

    render :json => @user.first_name + @user.last_name
  end

  def admin_create_account
    begin
      if (!User.find_by_email_address(params[:email_address]))
        @new_user = User.new(user_params)
        @new_user.email_address = @new_user.email_address.downcase.strip
        @new_user.active = true
        random_password = SecureRandom.hex 3
        @new_user.password = random_password

        if @new_user.save
          client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)

          client.deliver_with_template(
              {:from=>"salesdistributionlist@hvdiagnostics.com",
               :to=>"#{@new_user.email_address}",
               :template_id=>426261,
               :template_model=>
                   {"product_name"=>"HV Diagnostics",
                    "name"=>"#{@new_user.first_name}",
                    "username"=>"#{@new_user.email_address}",
                    "random_password"=>"#{random_password}",
                    "product_address_line1"=>"271 Rope Mill Parkway, Suite #2",
                    "product_address_line2"=>"Woodstock, GA 30188 USA",
                    "product_phone_number"=>"+1 (678) 445-2555",
                    "sender_name"=>"salesdistributionlist@hvdiagnostics.com"}
              }
          )

          render :json => {:status_code => 1, :msg => "Account has been created and activated. A temporary password has been emailed."}
        else
          render :json => {:status_code => -1, :msg => "Something went wrong. Account was not created."}
        end
      else
        render :json => {:status_code => -2, :msg => "This email address is already registered. Please pick another."}
      end
    rescue Exception => e
      logger.error e
    end
  end

  private
  def user_params
    params.require(:user).permit(:id, :updated_at, :first_name, :last_name, :middle_initial, :email_address, :password, :password_confirmation, :phone_number, :company_name, :street_address1, :street_address2, :city, :state, :country, :postal_code, :password_expires_on,:active, :created_at, :role_id, products:[:product_id, :serial_number, :name])
  end
end