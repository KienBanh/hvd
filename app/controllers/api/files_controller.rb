class Api::FilesController < ApplicationController
  def upload
    folder = "public/RMAs/Temp/"
    fileName = params[:file].original_filename
    path = Rails.root + folder + fileName

    logger.info path

    if !File.exists?(path)
      f = File.open File.join(folder, fileName), "wb"
      f.write params[:file].read()
      f.close
      render :json => {:status => 1, :msg => "File uploaded successfully.", :fileName => fileName, :path => "#{path}"}
    else
      render :json => "Filename conflict. Please rename your file.".to_json, :status => 409
    end
  end

  def download

  end

  def delete
    logger.info params

    FileUtils.rm(params[:path])

    render :json => 1
  end

  def delete_public_attachment
    split = params[:path].split("/")
    path =  Rails.root.join("public", split[1], split[2], split[3], split[4])

    if File.exists?(path)
      FileUtils.rm(path)
      Attachment.destroy(params[:id])

      render :json => 1
    else
      render :json => 0
    end
  end
end