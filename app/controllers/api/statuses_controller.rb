class Api::StatusesController < ApplicationController
  def create
    @status = Status.new(status_params)
  end

  def index
    @statuses = Status.all

    render :json => @statuses.to_json
  end

  private
  def status_params
    params.require(:status).permit(:id, :name, :description)
  end
end