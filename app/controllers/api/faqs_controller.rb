class Api::FaqsController < ApplicationController
  #before_action :set_faq, only: [:show, :edit, :update, :destroy]
  #before_action :authenticate_user, only: [:edit, :update, :destroy]
  before_action :is_admin, only: [:new, :edit, :update, :destroy, :create]

  # GET /faqs
  # GET /faqs.json
  def index
    @faqs = Faq.all

    render :json => @faqs
  end

  # GET /faqs/1
  # GET /faqs/1.json
  def show
  end

  # GET /faqs/new
  def new
    @faq = Faq.new
  end

  # GET /faqs/1/edit
  def edit
  end

  # POST /faqs
  # POST /faqs.json
  def create
  end

  # PATCH/PUT /faqs/1
  # PATCH/PUT /faqs/1.json
  def update
    if (faq_params[:id])
      @faq = Faq.find_by_id(faq_params[:id])

      if @faq.update(faq_params)

        render :json => {:status => 1, :msg => "FAQ was successfully updated.", :faq => @faq, :faqs => Faq.all}
      else
        render :json => {:status => -1, :msg => "Something went wrong. FAQ was not successfully updated.", :faq => @faq, :faqs => Faq.all}
      end
    else
      @faq = Faq.new(faq_params)

      if @faq.save
        render :json => {:status => 1, :msg => "FAQ was successfully updated.", :faq => @faq, :faqs => Faq.all}
      else
        render :json => {:status => -1, :msg => "Something went wrong. FAQ was not successfully updated.", :faq => @faq, :faqs => Faq.all}
      end
    end


  end

  # DELETE /faqs/1
  # DELETE /faqs/1.json
  def destroy
    @faq = Faq.find_by_id(params[:id])

    if @faq.destroy
      all_faqs_after = Faq.where("faq_order > ?", @faq.faq_order)

      if all_faqs_after
        all_faqs_after.each do |faq|
          faq.update_attribute(:faq_order, faq.faq_order - 1)
        end
      end

      render :json => {:status => 1, :msg => "FAQ was successfully updated.", :faqs => Faq.all}
    else
      render :json => {:status => -1, :msg => "Something went wrong. FAQ was not successfully deleted.", :faq => @faq, :faqs => Faq.all}
    end
  end

  def update_faq_order
    logger.info params
    @faq = Faq.find_by_id(faq_params[:id])
    # faqs_in_between = Faq.where(:faq_order => params[:endPos]..params[:startPos])
    faqs_in_between = Faq.where("faq_order <= ? AND faq_order >= ?", params[:startPos] > params[:endPos] ? params[:startPos] : params[:endPos], params[:endPos] > params[:startPos] ? params[:startPos] : params[:endPos])

    if @faq.faq_order == params[:endPos]
      render :json => {:status => 0, :msg => "No changes were made.", :faqs => Faq.all}
      return
    end

    begin
      if faqs_in_between.count == 2
        faqs_in_between.each do |loop_faq|
          if loop_faq.id != @faq.id
            loop_faq.update_attribute("faq_order", params[:startPos])

          else
            loop_faq.update_attribute("faq_order", params[:endPos])

          end
        end
      else
        faqs_in_between.each do |loop_faq|
          if loop_faq.id == @faq.id
            loop_faq.update_attribute("faq_order", params[:endPos])
          else
            if params[:endPos] > params[:startPos]
              loop_faq.update_attribute("faq_order", loop_faq.faq_order - 1)
            else
              loop_faq.update_attribute("faq_order", loop_faq.faq_order + 1)
            end
          end
        end
      end

      render :json => {:status => 1, :msg => "FAQ order was successfully updated.", :faqs => Faq.all}
    rescue
      render :json => {:status => -1, :msg => "FAQ order was NOT successfully updated.", :faqs => Faq.all}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  # def set_faq
  #   @faq = Faq.find(params[:id])
  # end

  # Never trust parameters from the scary internet, only allow the white list through.
  def faq_params
    params.require(:faq).permit(:id, :question, :answer, :order)
  end
end
