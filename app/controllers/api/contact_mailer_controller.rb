class Api::ContactMailerController < ApplicationController
  def create
    client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)

    status = client.deliver(
        from: 'salesdistributionlist@hvdiagnostics.com',
        to: 'salesdistributionlist@hvdiagnostics.com',
        subject: 'New Contact Email From Website',
        html_body: "
          <div>First Name: #{params[:firstName]}</div>
          <div>Middle Initial: #{params[:middleInitial]}</div>
          <div>Last Name: #{params[:lastName]}</div>
          <div>Email: #{params[:email]}</div>
          <div>Phone: #{params[:phone]}</div>
          <div>Address1: #{params[:address1]}</div>
          <div>Address2: #{params[:address2]}</div>
          <div>City: #{params[:city]}</div>
          <div>State: #{params[:state]}</div>
          <div>Postal Code: #{params[:postalCode]}</div>
          <div>Country: #{params[:country]}</div>
          <div>Message: #{params[:message]}</div>
        ",
        track_opens: true
    )

    render :json => status
  end
end