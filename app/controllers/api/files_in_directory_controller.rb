class Api::FilesInDirectoryController < ApplicationController
  def index
    logger.info params

    files = Dir.glob("public/#{params[:path]}/*").each do |file|
      file.sub!('public', '')
    end

    render :json => files
  end
end