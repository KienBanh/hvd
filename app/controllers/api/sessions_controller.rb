class Api::SessionsController < ApplicationController
  def login
    @user = User.find_by_email_address(login_params[:email_address].downcase.strip)

    if @user && @user.active && !@user.deleted && @user.authenticate(params[:password])
      $products = @user.products

      session[:user_id] = @user.id

      render :json => @user.to_json(:except => [:password, :password_digest])
    else
      render :json => -1
    end
  end

  def home
  end

  def profile
  end

  def setting
  end

  def reset_password
    @request = User.find_by_email_address(login_params[:email_address].downcase.strip)

    random_password = SecureRandom.hex 3

    @request.password = random_password
    @request.password_expires_on = DateTime.now
    @request.updated_at = DateTime.now

    if @request.save
      render :json => {:status_code => 1, :msg => "Password reset was successful."}

      client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)
      client.deliver_with_template(
          {:from=>"salesdistributionlist@hvdiagnostics.com",
           :to=>"#{@request.email_address}",
           :template_id=>108102,
           :template_model=>
               {"product_name"=>"HV Diagnostics",
                "name"=>"#{@request.first_name}",
                "temporary_password"=>"#{random_password}",
                "product_address_line1"=>"271 Rope Mill Parkway, Suite #2",
                "product_address_line2"=>"Woodstock, GA 30188 USA",
                "product_phone_number"=>"+1 (678) 445-2555"}
          }
      )
    else
      render :json => {:status_code => -1, :msg => "Something went wrong. Password reset was not successful."}
    end
  end

  private
  def login_params
    params.require(:session).permit(:email_address, :password, :validations)
  end
end