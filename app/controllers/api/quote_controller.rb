class Api::QuoteController <ApplicationController
  def create
    client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)

    status = client.deliver(
        from: 'salesdistributionlist@hvdiagnostics.com',
        to: 'salesdistributionlist@hvdiagnostics.com',
        subject: 'New Quote Email From Website',
        html_body: "
          <div>Company: #{params[:company]}</div>
          <div>Name: #{params[:name]}</div>
          <div>Email: #{params[:email]}</div>
          <div>Phone: #{params[:phone]}</div>
          <div>Products: #{params[:products]}</div>
        ",
        track_opens: true
    )

    render :json => status
  end
end