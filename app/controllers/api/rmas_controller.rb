class Api::RmasController < ApplicationController
  before_action :authenticate_user, only: [:show]
  before_action :is_admin, only: [:index, :destroy, :edit, :update, :destroy]

  def index
    @user = User.find_by_id(session[:user_id])

    if @user.role_id == 2
      @rmas = Rma.where(active: true).order('rma_number').reverse_order
    else
      @rmas = @user.rmas.where(active: true).order('rma_number').reverse_order
    end

    if (@rmas)
      render :json => {:status => 1, :msg => "", :rmas => @rmas}
    else
      render :json => {:status => -1, :msg => "", :rmas => {}}
    end
  end

  def new
    render :json => "ok"
  end

  def show
    @rma = Rma.find_by_id(params[:id])
    @attachments = @rma.attachments
    @received_parts = @rma.received_parts
    @user = User.find_by_id(session[:user_id])

    if (@user.role_id != 2 && @rma.users[0].id != @user.id)
      render :json => {:status => -1, :msg => "You don't have rights to view this RMA."}

      return
    end

    if @user.role_id == 2
      @messages = @rma.messages
      @messages = @messages.as_json
      @used_parts = @rma.used_parts

      @messages.each do |message|
        message_added_by = User.find_by_id(message["user_id"])
        message[:first_name] = message_added_by.first_name
        message[:last_name] = message_added_by.last_name
      end

      render :json => {:status => 1, :msg => "", :rma => @rma, :attachments => @attachments, :received_parts => @received_parts, :rmaMessages => @messages, :used_parts => @used_parts}
    else
      #if not admin
      @messages = @rma.messages.where("private IS NULL OR private = ?", false)
      @messages = @messages.as_json

      @messages.each do |message|
        message_added_by = User.find_by_id(message["user_id"])
        message[:first_name] = message_added_by.first_name
        message[:last_name] = message_added_by.last_name
      end

      render :json => {:status => 1, :msg => "", :rma => @rma, :attachments => @attachments, :received_parts => @received_parts, :rmaMessages => @messages}
    end
  end

  def create
    @user = User.find_by_id(params[:user_id])
    @rma = Rma.new(rma_params)

    if (Rma.count > 0)
      @rma.rma_number = Rma.last.id + 8000
    else
      @rma.rma_number = 8000
    end

    if (@rma.product_id > 0)
      @rma.product_name = Product.find_by_id(@rma.product_id).name
    end

    if @user.rmas << @rma
      FileUtils.mkdir_p("#{Rails.root}/public/RMAs/#{@rma.rma_number}/public_attachments") unless File.directory?("#{Rails.root}/public/RMAs/#{@rma.rma_number}/public_attachments")
      FileUtils.mkdir_p("#{Rails.root}/public/RMAs/#{@rma.rma_number}/internal_attachments") unless File.directory?("#{Rails.root}/public/RMAs/#{@rma.rma_number}/internal_attachments")

      if params[:attachments]
        params[:attachments].each do |attachment|
          logger.info attachment[:path]
          FileUtils.mv("#{attachment[:path]}", "#{Rails.root}/public/RMAs/#{@rma.rma_number}/public_attachments/")
          @rma.attachments.build({rma_id: @rma.id, user_id: @user.id, path: "/RMAs/#{@rma.rma_number}/public_attachments/#{attachment[:fileName]}"}).save
        end
      end

      render :json => {:status => 1, :msg => "RMA was successfully generated.", :rma => @rma}

      client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)
      newRmaEmailToAdmin = client.deliver(
          from: 'servicesupport@hvdiagnostics.com',
          to: 'servicesupport@hvdiagnostics.com',
          subject: "New RMA Created: ##{@rma.rma_number}",
          html_body: "<div>A RMA has been created. RMA ##{@rma.rma_number}</div>",
          track_opens: true
      )

      newRmaEmailToClient = client.deliver_with_template(
          {
              :from=>"servicesupport@hvdiagnostics.com",
              :to=>@rma.email_address,
              :template_id=>246741,
              :template_model=>
                  {
                      "product_name"=>"HV Diagnostics",
                      "total"=>"total_Value",
                      "name"=>@rma.first_name,
                      "rma_number"=>@rma.rma_number,
                      "product_phone_number"=>"+1 (678) 445-2555",
                      "rma_link" => "http://hvdiagnostics.com/account/rmas/edit/#{@rma.id}",
                  }
          }
      )
    else
      render :json => {:status => -1, :msg => "Something went wrong. RMA was not successfully generated."}
    end
  end

  def update
    @rma = Rma.find_by_id(rma_params[:id])
    @messages = @rma.messages
    @rma_user = @rma.users[0]
    @rma_user.last_rma_update_on = Time.at(0) if @rma_user.last_rma_update_on.blank?
    begin    
      if rma_params
        puts "from form:" + rma_params[:status_id].to_s + ", from db: " + @rma.status_id.to_s      
        
        @rma.update_attributes(rma_params)

        @rma = Rma.find_by_id(rma_params[:id])

        if rma_params[:status_id] == 2 && @rma.on_bench_on.nil?
          @rma.update_attribute(:on_bench_on, Time.now)
        end

        if rma_params[:status_id] === 5
          @rma.update_attribute(:shipped_back_on, Time.now)
        end
      end

      if (params[:received_parts])
        ReceivedPart.delete_all "rma_id = #{@rma.id}"

        params[:received_parts].each do |part|
          @rma.received_parts.build({part: part["part"], other: part["other"]}).save
        end
      end

      if (params[:rmaMessage])
        @rma.messages.build(message: params[:rmaMessage][:message], private: params[:rmaMessage][:private], user_id:  params[:rmaMessage][:user_id]).save
        @messages = @rma.messages
        private_message = params[:rmaMessage][:private]
      else
        private_message = false
      end

      if ((Time.now - @rma_user.last_rma_update_on) / 3600 > 8) && !private_message
        client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)
        newRmaEmailToClient = client.deliver_with_template(
            {
                :from=>"servicesupport@hvdiagnostics.com",
                :to=>@rma.email_address,
                :template_id=>312002,
                :template_model=>
                    {
                        "product_name"=>"HV Diagnostics",
                        "name"=>@rma.first_name,
                        "rma_number"=>@rma.rma_number,
                        "product_phone_number"=>"+1 (678) 445-2555",
                        "rma_link" => "http://hvdiagnostics.com/account/rmas/edit/#{@rma.id}",
                    }
            }
        )       

        @rma_user.last_rma_update_on = Time.now
        @rma_user.save
      end

      render :json => {:status => 1, :msg => "RMA was successfully updated.", :rma => @rma, :received_parts => @rma.received_parts, :rmaMessages => @messages}
    rescue => ex
      logger.info ex
      render :json => {:status => -1, :msg => "RMA was NOT successfully updated."}
    end
  end

  def add_new_attachment
  end

  def send_update_email
    @rma = Rma.find_by_id(params[:id])

    logger.info @rma.inspect

    client = Postmark::ApiClient.new('93ec34ab-f757-4cf6-8355-7c34863c2407', secure: false)
    newRmaEmailToClient = client.deliver_with_template(
        {
            :from=>"servicesupport@hvdiagnostics.com",
            :to=>@rma.email_address,
            :template_id=>312002,
            :template_model=>
                {
                    "product_name"=>"HV Diagnostics",
                    "name"=>@rma.first_name,
                    "rma_number"=>@rma.rma_number,
                    "product_phone_number"=>"+1 (678) 445-2555",
                    "rma_link" => "http://hvdiagnostics.com/account/rmas/edit/#{@rma.id}",
                }
        }
    )

    logger.info newRmaEmailToClient

    if (newRmaEmailToClient[:error_code] == 0)
      render :json => {:status => 1, :msg => "Update email was sent."}
    else
      render :json => {:status => -1, :msg => "Update email was not sent."}
    end
  end

  def public_upload
    @rma = Rma.find_by_id(params[:id])
    folder = "public/RMAs/#{@rma.rma_number}/public_attachments"
    fileExtension = params[:file].content_type.split("/")[1]
    fileName = params[:file].original_filename
    path = Rails.root + folder + fileName

    if !File.exists?(path)
      begin
        f = File.open File.join(folder, fileName), "wb"
        f.write params[:file].read()
        f.close

        @rma.attachments.build({rma_id: @rma.id, user_id: session[:user_id], path: "/RMAs/#{@rma.rma_number}/public_attachments/#{fileName}"}).save
        @attachments = @rma.attachments
        render :json => {:status => 1, :msg => "File uploaded successfully.", :fileName => fileName, :attachments => @attachments}
      rescue => err
        logger.fatal(err)

        render :json => {:status => -9999, :msg => err, :fileName => fileName, :attachments => @attachments}
      end
    else
      @attachments = @rma.attachments

      render :json => {:status => -1, :msg => "Filename conflict. Please rename your file.", :fileName => fileName, :attachments => @attachments}
    end
  end

  def delete_rma
    @rma = Rma.find_by_id(params[:id])

    begin
      @rma.update_attribute(:active, false)

      render :json => {:status => 1, :msg => "RMA was successfully deleted."}
    rescue
      render :json => {:status => -1, :msg => "RMA was not successfully deleted."}
    end
  end

  def create_used_part
    @rma = Rma.find_by_id(params[:id])

    if params[:file]
      folder = "public/RMAs/#{@rma.rma_number}/internal_attachments"
      fileExtension = params[:file].content_type.split("/")[1]
      fileName = params[:file].original_filename
      path = Rails.root + folder + fileName

      if !File.exists?(path)
        begin
          f = File.open File.join(folder, fileName), "wb"
          f.write params[:file].read()
          f.close
        rescue => err
          logger.fatal err

          render :json => {:status => -9999, :msg => err, :fileName => fileName, :used_parts => @rma.used_parts}
        end

        @rma.used_parts.build({rma_id: @rma.id, user_id: session[:user_id], path: "/RMAs/#{@rma.rma_number}/internal_attachments/#{fileName}", name: params[:name], description: params[:description]}).save
        @used_parts = @rma.used_parts
        render :json => {:status => 1, :msg => "File uploaded successfully.", :fileName => fileName, :used_parts => @used_parts}
      else
        @used_parts = @rma.used_parts

        render :json => {:status => -1, :msg => "Filename conflict. Please rename your file.", :fileName => fileName, :used_parts => @used_parts}
      end
    else
      @rma.used_parts.build({rma_id: @rma.id, user_id: session[:user_id], name: params[:name], description: params[:description]}).save

      @used_parts = @rma.used_parts

      render :json => {:status => 1, :msg => "File uploaded successfully.", :fileName => fileName, :used_parts => @used_parts}
    end
  end

  protected
  def rma_params
    params.require(:rma).permit(:id, :rma_number, :user_id, :first_name, :last_name, :email_address, :company_name, :on_bench_on, :shipped_back_on, :phone_number, :shipping_address1, :shipping_address2, :shipping_city, :shipping_state, :shipping_country, :shipping_postal_code, :shipping_carrier, :shipping_method, :shipping_value, :shipping_insurance_required, :shipping_account_number, :billing_address1, :billing_address2, :billing_city, :billing_state, :billing_country, :billing_postal_code, :product_id, :product_name, :model_number, :serial_number, :calibration_required, :purchase_date, :description, :status_id, :received_on, :equipment_received, :condition, :shipping_material, :hours_spent, :rate, :parts_labor_cost, :calibration_cost, :total_cost, :repair_tech, :repair_completed_on, :updated_at, :created_at, :weight, :dimensions, :packed_by, :invoice_number, :attachments, :expedite, :condition_packaging)
  end

  def received_parts_params
    params.require(:received_parts).permit(:part, :other)
  end
end