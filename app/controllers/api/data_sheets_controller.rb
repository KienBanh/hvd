class Api::DataSheetsController < ApplicationController
  respond_to :json

  def index
    data_sheets = {}
    Dir.entries("public/Data Sheets/").each do |folder|
      if folder == "." || folder == ".."
      else
        files = Dir.glob("public/Data Sheets/#{folder}/*").each do |file|
          file.sub!('public/', '')
        end
        folder.gsub!('_', '/')
        data_sheets[folder] = files
      end
    end
    render :json => data_sheets
  end
end