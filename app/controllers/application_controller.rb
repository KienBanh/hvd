class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index
    render "layouts/application"
  end

  protected
  def authenticate_user
    if session[:user_id]
      @user = User.find(session[:user_id])

      if @user.active == true
        # set current user object to @current_user object variable
        #@current_user = User.find session[:user_id]
        return true
      else
        return false
      end
    else
      redirect_to(:controller => 'application', :action => 'index')
      return false
    end
  end

  def save_login_state
    if session[:user_id]
      #redirect_to(:controller => 'sessions', :action => 'home')
      return false
    else
      return true
    end
  end

  def is_admin
    if session[:user_id]
      @user = User.find(session[:user_id])

      if @user.role_id == 2
        # set current user object to @current_user object variable
        #@current_user = User.find session[:user_id]
        return true
      else
        return false
      end
    else
      redirect_to(:controller => 'application', :action => 'index')
      return false
    end
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize
    redirect_to '/login' unless current_user
  end
end