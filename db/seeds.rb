# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create(:first_name => "Kien", :last_name => "Banh", :email_address => "kien.banh@skyeds.com", :password_salt => "1", :password => "1", :phone_number => "4049818305", :company => "Skyeds", :street_address1 => "4512 Treadstone Ct.", :city => "Suwanee", :state => "GA", :country => "US", :postal_code => "30024", :security_question1 => "1", :security_answer1 => "1", :security_question2 => "2", :security_answer2 => "2", :security_question3 => "3", :security_answer3 => "3", :role_id => "2")