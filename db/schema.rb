# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190908203014) do

  create_table "attachments", force: :cascade do |t|
    t.integer  "rma_id"
    t.integer  "user_id"
    t.string   "path"
    t.string   "url"
    t.boolean  "private",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "attachments", ["rma_id"], name: "index_attachments_on_rma_id"
  add_index "attachments", ["user_id"], name: "index_attachments_on_user_id"

  create_table "categories", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faqs", force: :cascade do |t|
    t.string   "question"
    t.string   "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at"
    t.integer  "faq_order"
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "rma_id"
    t.string   "message"
    t.boolean  "private"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "messages", ["rma_id"], name: "index_messages_on_rma_id"

# Could not dump table "products" because of following NoMethodError
#   undefined method `[]' for nil:NilClass

  create_table "products_users", id: false, force: :cascade do |t|
    t.integer "product_id",    null: false
    t.integer "user_id",       null: false
    t.string  "serial_number"
  end

  add_index "products_users", ["product_id"], name: "index_products_users_on_product_id"
  add_index "products_users", ["user_id"], name: "index_products_users_on_user_id"

  create_table "received_parts", force: :cascade do |t|
    t.integer  "rma_id"
    t.string   "part"
    t.boolean  "other",      default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "received_parts", ["rma_id"], name: "index_received_parts_on_rma_id"

# Could not dump table "rmas" because of following NoMethodError
#   undefined method `[]' for nil:NilClass

  create_table "rmas_users", id: false, force: :cascade do |t|
    t.integer "rma_id",  null: false
    t.integer "user_id", null: false
  end

  add_index "rmas_users", ["rma_id"], name: "index_rmas_users_on_rma_id"
  add_index "rmas_users", ["user_id"], name: "index_rmas_users_on_user_id"

  create_table "roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "used_parts", force: :cascade do |t|
    t.integer  "rma_id"
    t.string   "name"
    t.string   "description"
    t.string   "path"
    t.string   "url"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
  end

  add_index "used_parts", ["rma_id"], name: "index_used_parts_on_rma_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name",                          null: false
    t.string   "last_name",                           null: false
    t.string   "middle_initial"
    t.string   "email_address",                       null: false
    t.string   "password_digest",                     null: false
    t.string   "password"
    t.string   "phone_number",                        null: false
    t.string   "company_name",                        null: false
    t.string   "street_address1",                     null: false
    t.string   "street_address2"
    t.string   "city",                                null: false
    t.string   "state"
    t.string   "country",                             null: false
    t.string   "postal_code",                         null: false
    t.integer  "role_id",             default: 1
    t.boolean  "active",              default: false
    t.datetime "password_expires_on"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.datetime "last_rma_update_on"
    t.boolean  "deleted",             default: false, null: false
  end

end
