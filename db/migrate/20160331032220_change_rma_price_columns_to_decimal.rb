class ChangeRmaPriceColumnsToDecimal < ActiveRecord::Migration
  def change
    change_column :rmas, :hours_spent, :decimal
    change_column :rmas, :rate, :decimal
    change_column :rmas, :parts_labor_cost, :decimal
    change_column :rmas, :calibration_cost, :decimal
  end
end
