class AddActiveToRmas < ActiveRecord::Migration
  def change
	add_column :rmas, :active, :bool, :default => true, :null => false
  end
end
