class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :middle_initial
      t.string :email_address, null: false
      #t.string :password_hash, null: false
      t.string :password_digest, null: false
      t.string :password
      t.string :phone_number, null: false
      t.string :company_name, null: false
      t.string :street_address1, null: false
      t.string :street_address2
      t.string :city, null: false
      t.string :state, null: false
      t.string :country, null: false
      t.string :postal_code, null: false
      t.references :role, default: 1
      t.boolean :active, default: false
      t.datetime :password_expires_on
      t.timestamps null: false
    end
  end
end