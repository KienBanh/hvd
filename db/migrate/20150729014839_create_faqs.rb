class CreateFaqs < ActiveRecord::Migration
  def up
    create_table :faqs do |t|
      t.string :question
      t.string :answer
      t.timestamps null: false
      t.datetime "updated_at"
      t.integer :faq_order
    end

    Faq.create :question => 'WHY IS THERE A DIFFERENCE SOMETIMES IN RESISTANCE VALUE BETWEEN A DC MEGA OHMMETER TEST AND A VLF AC RESISTANCE VALUE?',
               :answer => 'DC Resistance measurements (Mega Ohmmeter) measure the current drawn under an applied voltage and divide this current value into the applied voltage to get the resultant so called insulation resistance (ohms law).<br/><br/>The HVA line of VLF/DC test instruments, unlike many other VLF units on the market, also measure the resistance as well as the capacitance of the load. Note the resistance value displayed is not the impedance of the load but it is the actual real component of the overall current. The real component IR of the total current IT is then divided into the applied AC RMS voltage to give the resultant insulation resistance.<br/><br/>The capacitance of loads like cables play a very small role when measuring steady state resistive leakage currents under DC, but play a major role under AC (including VLF) conditions.<br/><br/>Therefore the real resistance value of the load under AC applied voltages may not be the same as the resistance of the load under applied DC voltages. Most cables are naturally under AC applied voltages when in normal operation.'

    Faq.create :question => 'HOW DO I FIND THE CORRECT COM PORT FOR THE HVA OR TD CONTROL CENTER SOFTWARE?',
               :answer => 'There are a few ways to find the correct COM port depending on what operating system your computer is using (Windows XP, Vista, Windows 7).<br/><br/>You need to look at the Device Manager to locate the Com port allocated to the Com port in use for that particular device. Note that if you are using a USB to Serial cable adapter, you will generally be required to install a driver for that adapter.'

    Faq.create :question => 'HOW CAN I CHECK IF MY HIGH VOLTAGE TEST LEAD IS GOOD FOR A HVA SERIES TEST INSTRUMENT?',
               :answer => 'Test leads generally get a lot of abuse during their lifetime and are often therefore subject to failure. They often get more abused than the test equipment they connect to.<br/><br/>While having a broken test lead can lead to some frustrating troubleshooting to find the guilty culprit, there are some basic tests that can be done to either help eliminate or blame the test lead for a possible faulty test result.<br/></br>Using a conventional multimeter in Ohm mode, check the resistance of the main conductor and shield. Note: The test lead should be removed from the test equipment and obviously not energized for these tests to be carried out.<br/><br/>For the HVA30/34:<br/>The main conductor should have less than a few Ohms from from one end to the other end - that is full continuity. The Shield should also be continuous (few ohms) from end to end.<br/><br/>For the HVA60: Depending on the version of test lead, the main conductor should have either about 10k ohms or just less than 3k ohms from end to end.<br/><br/>The Shield should be continuous (few ohms) from end to end.'

    Faq.create :question => 'HOW DO YOU CHANGE THE DATA IN THESE REPORTS?',
               :answer => 'You can not change the test data measured by the HVA in the reports. This is important from a security perspective to prevent inadvertent or deliberate modification of measured test data.<br/><br/>The only items that can be changed are non measured user defined data such as cable length, manufacturer, names, work order_in_list  etc.'

    Faq.create :question => 'HOW LONG A CABLE CAN I TEST?',
               :answer => "The length of cable you can test depends on the cable that is being tested and the capabilities of the test instrument being used.
Regarding the cable being tested, what is of primary importance is the capacitance of the cable. This value can be obtained from the cable's data sheet or by asking the cable manufacturer. This is normally given as per ft or per meter. Simply multiply this value by the distance of the cable in question and you have your capacitance.<br/><br/>Next look at the capabilities of the test instrument and the voltage and frequency that you will want to apply to the cable.<br/><br/>Example:<br/>The HVA34 is one of the most popular VLF units on the market. It can test 0.5uF (0.5E-6 F) at full voltage (24kV RMS) and max. frequency (which is 0.1Hz).<br/><br/>If the cable that is to be tested has a capacitance of 100pF (100E-12 F) per ft, what is the maximum length of cable that can be tested using this HVA34 test instrument at full voltage and freq.<br/><br/>So 0.5E-6/100E-12 = 5000 ft of cable!<br/><br/>Remember that if a lower frequency is selected, such as 0.05Hz, this will be 10 000 ft of cable (half freq, double the length).<br/><br/>Also note that if the voltage is decreased, the length of cable can also be increased. If for example 16kV is applied, then a cable of about 11000 ft can be tested."

    Faq.create :question => 'HOW DO I CONNECT VIA BLUETOOTH TO MY DEVICE?',
               :answer => 'The items that use Bluetooth are the Tan Delta Series, the HVA28TD, and the BA Series.<br/><br/><b>If you have Windows 7:</b><br/>To connect to your Bluetooth device using your bluetooth dongle, insert the dongle into your computer. After the bluetooth icon displays on the lower right hand side of your computer screen, right click on it. Select "Add a device". If your device (TD, HVA28TD, BA) does not show its serial number, you will see "Handheld device". Select your device and click "Next". You may get an error screen telling you that you can not connect. Retry adding your device typically two to three times. This normally does the trick. It will finally accept it.<br/><br/>
Also, you can try this video: http://windows.microsoft.com/en-us/windows7/connect-to-bluetooth-and-other-wireless-or-network-devices.<br/><br/>If you have Windows XP, can you try this site: http://support.microsoft.com/kb/883259'

    Faq.create :question => 'WHAT IS THE BLUETOOTH PASSWORD?',
               :answer => 'The password to connect to any of our units (Tan Delta, HVA28TD, BA series) is "welcome" in all lower case letters.'

    Faq.create :question => 'HOW LONG / SHORT CAN THE CABLE BE IN order_in_list TO PERFORM A TAN DELTA TEST?',
               :answer => "Note: to perform a VLF AC test with our equipment you can test as short a cable as possible as long as it is properly terminated. <br />
<br />
However due to limitations based on very low loads resulting in very low current draws from the load, very short cable can pose a limitation for a TD diagnostic test . <br />
<br />
The length limitation is dependant on two parameters: the cable's capacitance (that is length dependant) and the test instrument being used for the test.<br />
<br />
How short ?<br />
The Tan Delta needs to be a minimum of 5nF to have sufficient current to run a test. To find the minimum length, divide 5nF by the capacitance per foot of your cable (contact manufacturer). On average, the capacitance per foot is 0.1nF/ft. The resulting number will give you the minimum needed to test.<br />
<br />
Example: 5nF / (0.1nF/ft) = 50 ft of cable.<br />
<br />
How long ?<br />
At full voltage and frequency The HVA28/HVA28TD can test up to 5,000ft; HVA34 can test up to 5,000 ft; HVA30-5 can test up to 38,000 ft; HVA60 can test up to 10,000 ft; and the HVA90 can test up to 10,000 ft. At reduced frequencies and voltages, the units can test further.<br />
<br />
Note: 1 mile = 5,280 ft."

    Faq.create :question => 'IS AN AC TEST A DESTRUCTIVE TEST?',
               :answer => "Correct AC VLF testing of cables has been proven to be a more effective method of testing MV cables that the traditional DC test that is no longer recommended or approved. It has the ability to find defects in a cable without making the cable weaker."

    Faq.create :question => 'WHAT IS VLF?',
               :answer => 'VLF stands for &quot;Very Low Frequency&quot;, which is 0.1Hz. One of the benefits of using VLF is that a VLF unit requires 600 times less power than a machine that operates at 60Hz. Another advantage of VLF is the unit can be constructed smaller and more portable than a 60Hz unit.'

    Faq.create :question => 'HOW OFTEN DOES EACH UNIT NEED CALIBRATION?',
               :answer => 'The HVA series, BA series, and TD series all have to be calibrated annually.'

    Faq.create :question => 'CAN THE HVA FIND THE LOCATION OF A FAULT?',
               :answer => 'The HVA cannot not find the location of a fault. It can be used to estimate the length of the cable if the capacitance is known or help condition a fault if fault location needs to be performed. Also, HVA series has the ability &quot;burn on arc&quot; for up to 5 minutes to help find a fault.<br />
<br />
The TDR may help find the fault. HV Diagnostics also can provide fault location services.'

    Faq.create :question => 'WHERE CAN I FIND THE TEST VOLTAGES FOR A WITHSTAND TEST?',
               :answer => 'The IEEE table 400.2 provides the test voltages for an AC Withstand test. The voltage level is dependent on what type of test is being performed: an installation test, an acceptance test, or a maintenance test. Also, be aware that peak voltages are right next to the RMS voltages.'

    Faq.create :question => 'CAN I PERFORM A TD TEST IF THERE ARE SPLICES IN THE CABLE?',
              :answer => 'Yes, a Tan Delta test can still be performed on a cable with splices.'

    Faq.where(:id => 1).update_all(:faq_order => 1)
    Faq.where(:id => 2).update_all(:faq_order => 2)
    Faq.where(:id => 3).update_all(:faq_order => 3)
    Faq.where(:id => 4).update_all(:faq_order => 4)
    Faq.where(:id => 5).update_all(:faq_order => 5)
    Faq.where(:id => 6).update_all(:faq_order => 6)
    Faq.where(:id => 7).update_all(:faq_order => 7)
    Faq.where(:id => 8).update_all(:faq_order => 8)
    Faq.where(:id => 9).update_all(:faq_order => 9)
    Faq.where(:id => 10).update_all(:faq_order => 10)
    Faq.where(:id => 11).update_all(:faq_order => 11)
    Faq.where(:id => 12).update_all(:faq_order => 12)
    Faq.where(:id => 13).update_all(:faq_order => 13)
    Faq.where(:id => 14).update_all(:faq_order => 14)
  end

  def down
    drop_table :faqs
  end
end