class ChangeCategoryDataSheetNullable < ActiveRecord::Migration
  def change
  	change_column_null :products, :data_sheet, true
  end
end
