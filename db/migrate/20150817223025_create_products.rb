class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :category, null: false
      t.string :name, null: false
      t.string :data_sheet, null: false
      t.string :serial_number
      t.timestamps null: false
    end

    create_join_table :products, :users do |t|
      t.index :product_id
      t.index :user_id
      t.string :serial_number
    end
  end
end