class AdditionalStatuses < ActiveRecord::Migration
  def change
  	Status.create("name" => "Repair Report Sent")
	Status.create("name" => "Repair Approved")
  end
end
