class AddUserIdToUsedParts < ActiveRecord::Migration
  def change
    add_column :used_parts, :user_id, :integer
  end
end
