class UserStateNullable < ActiveRecord::Migration
  def change
    change_column :users, :state, :string, :null => true
  end
end
