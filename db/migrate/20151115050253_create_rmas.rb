class CreateRmas < ActiveRecord::Migration
  def change
    create_table :rmas do |t|
      t.string :rma_number
      t.string :first_name
      t.string :last_name
      t.string :email_address
      t.string :company_name
      t.string :phone_number
      t.string :shipping_address1
      t.string :shipping_address2
      t.string :shipping_city
      t.string :shipping_state
      t.string :shipping_country
      t.string :shipping_postal_code
      t.string :shipping_carrier
      t.string :shipping_method
      t.float :shipping_value
      t.boolean :shipping_insurance_required
      t.string :shipping_account_number
      t.string :billing_address1
      t.string :billing_address2
      t.string :billing_city
      t.string :billing_state
      t.string :billing_country
      t.string :billing_postal_code
      t.integer :product_id
      t.string :product_name
      t.string :model_number
      t.string :serial_number
      t.boolean :calibration_required
      t.string :purchase_date
      t.string :description
      t.integer :status_id, :default => 7
      t.datetime :received_on
      t.string :equipment_received
      t.string :condition
      t.string :shipping_material
      t.integer :hours_spent
      t.integer :rate
      t.integer :parts_labor_cost
      t.integer :calibration_cost
      t.integer :total_cost
      t.string :repair_tech
      t.datetime :repair_completed_on
      t.timestamps null: false
      t.integer :weight
      t.string :dimensions
      t.integer :packed_by
      t.integer :invoice_number
    end

    create_table :received_parts do |t|
      t.belongs_to :rma, index:true
      t.string :part
      t.boolean :other, :default => false
      t.timestamps null: false
    end

    create_table :attachments do |t|
      t.belongs_to :rma, index:true
      t.belongs_to :user, index:true
      t.string :path
      t.string :url
      t.boolean :private, :default => false
      t.timestamps null: false
    end

    # create_table :shipping_materials do |t|
    #   t.index :rma_id
    # end

    # create_table :conditions do |t|
    #   t.index :rma_id
    # end

    create_table :messages do |t|
      t.belongs_to :rma, index:true
      t.string :message
      t.boolean :private
      t.integer :user_id
      t.timestamps null: false
    end

    create_table :used_parts do |t|
      t.belongs_to :rma, index:true
      t.string :name
      t.string :description
      t.string :path
      t.string :url
      t.timestamps null: false
    end

    # create_join_table :rmas, :messages do |t|
    #   t.index :rma_id
    #   t.index :message_id
    # end

    create_join_table :rmas, :users do |t|
      t.index :rma_id
      t.index :user_id
    end

    create_table :statuses do |t|
      t.string :name
      t.string :description
      t.timestamps null: false
    end

    Status.create :name => "Received"
    Status.create :name => "On Bench"
    Status.create :name => "Waiting on Parts"
    Status.create :name => "Waiting on Customer"
    Status.create :name => "Shipped Back"
    Status.create :name => "Completed"
    Status.create :name => "Submitted"

    add_foreign_key :rmas, :statuses
  end
end