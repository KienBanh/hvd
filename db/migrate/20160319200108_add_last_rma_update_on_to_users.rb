class AddLastRmaUpdateOnToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_rma_update_on, :datetime
  end
end
