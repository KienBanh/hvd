class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name, null: false
      t.timestamps null: false
    end

    Category.create :name => "HVA"
    Category.create :name => "TD"
    Category.create :name => "PD"
    Category.create :name => "EZ-Cable ID"
    Category.create :name => "TDR"
    Category.create :name => "BA"
    Category.create :name => "Current Injectors"
    Category.create :name => "Micro-Ohmmeters"
  end
end