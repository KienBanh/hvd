class AddExpediteToRmas < ActiveRecord::Migration
  def change
    add_column :rmas, :expedite, :bool
  end
end
