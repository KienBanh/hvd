class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name, null: false
      t.timestamps null: false
    end

    Role.create :name => "user"
    Role.create :name => "admin"
  end
end
